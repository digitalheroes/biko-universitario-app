import React, { Component } from 'react'
import { Dimensions, View, ScrollView, Image } from 'react-native'
import { A, H2, P, Strong, Button } from 'nachos-ui'
import MapView, { Marker } from 'react-native-maps'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import { connect } from 'react-redux'

import { fontFamily } from '../../theme'
import jobImage from '../../assets/images/pin_image.png'
import mapStyle from '../../assets/mapStyle'
import { fetchJobDetail } from '../../modules/campings'

class JobDetail extends Component {
  static navigationOptions = {
    title: 'Detalhes da vaga'
  }

  constructor (props) {
    super(props)
    const { id } = this.props.navigation.getParam('job')
    const { fetchJobDetail } = this.props
    fetchJobDetail(id)
  }

  render () {
    const { width } = Dimensions.get('screen')
    const { jobDetail } = this.props
    const job = this.props.navigation.getParam('job')
    const {
      applied,
      jobTitle,
      jobDescription,
      jobRemuneration,
      jobType,
      jobCheckIn,
      jobCheckOut,
      Company: { phantasyName, Address: { latitude, longitude } }
    } = jobDetail || job
    return (
      <ScrollView style={{ flex: 1 }}>
        <MapView
          style={{ flex: 3, height: 200, width }}
          initialRegion={{
            latitude: parseFloat(latitude),
            longitude: parseFloat(longitude),
            latitudeDelta: 0.08,
            longitudeDelta: 0.09
          }}
          customMapStyle={mapStyle}
        >
          <Marker coordinate={{ latitude: parseFloat(latitude), longitude: parseFloat(longitude) }}>
            <Image source={jobImage} style={[{ maxHeight: 46 }]} resizeMode='contain' />
          </Marker>
        </MapView>
        <View style={{ flex: 5 }}>
          <View style={{ alignItems: 'flex-start', justifyContent: 'flex-start', marginVertical: 20, paddingHorizontal: 20 }}>
            <View style={{ backgroundColor: '#46e484', paddingVertical: 5, paddingHorizontal: 5, borderRadius: 4 }}>
              <Strong style={{ color: '#ffffff', paddingVertical: 0 }}>{jobType} • R$ {jobRemuneration} / h</Strong>
            </View>
            <H2 style={{ color: '#37455c', fontFamily: fontFamily.palanquin.bold, fontWeight: '600' }}>{jobTitle}</H2>
            <View style={{ flexDirection: 'row' }}>
              <A><FontAwesome5 name='building' />{phantasyName}</A>
            </View>
          </View>
          <View style={{ backgroundColor: '#fafafb', paddingHorizontal: 20 }}>
            <View>
              <Strong style={{ color: '#53667d', fontWeight: '700', fontFamily: fontFamily.palanquin.bold, paddingBottom: 0 }}>Descrição</Strong>
              <P style={{ color: '#a8b1bd' }}>{jobDescription}</P>
            </View>
            <View>
              <Strong style={{ color: '#53667d', fontWeight: '700', fontFamily: fontFamily.palanquin.bold, paddingBottom: 0 }}>Endereço</Strong>
              <P style={{ color: '#a8b1bd' }} numberOfLines={2}>Avenida Paulista, 1765 - Bela Vista, São Paulo</P>
            </View>
            <View>
              <Strong style={{ color: '#53667d', fontWeight: '700', fontFamily: fontFamily.palanquin.bold, paddingBottom: 0 }}>Entrada e saída</Strong>
              <P style={{ color: '#a8b1bd' }} numberOfLines={2}>{jobCheckIn} às {jobCheckOut}</P>
            </View>
            <View>
              <Strong style={{ color: '#53667d', fontWeight: '700', fontFamily: fontFamily.palanquin.bold, paddingBottom: 0 }}>Habilidades</Strong>
              <P style={{ color: '#a8b1bd' }} numberOfLines={2}>{job.Skills.map(skill => skill.habilityName)}</P>
            </View>
          </View>
          <View style={{ marginVertical: 10, paddingHorizontal: 20 }}>
            {applied ? (
              <View style={{ alignItems: 'center' }}><P>Você já se candidatou para essa vaga.</P></View>
            ) : (
              <Button onPress={() => this.props.navigation.navigate('SendApplication', { job })}>Enviar candidatura</Button>
            )}
          </View>
        </View>
      </ScrollView>
    )
  }
}

JobDetail.propTypes = {}

const mapStateToProps = state => ({
  jobDetail: state.jobDetail
})

const mapActionsToProps = {
  fetchJobDetail
}

export default connect(mapStateToProps, mapActionsToProps)(JobDetail)
