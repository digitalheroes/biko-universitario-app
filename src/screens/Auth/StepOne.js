import moment from 'moment'
import React, { Component, Fragment } from 'react'
import {
  KeyboardAvoidingView,
  StatusBar,
  StyleSheet,
  View
} from 'react-native'
import { Formik } from 'formik'
import { Button, H2, P } from 'nachos-ui'
import { MaskService } from 'react-native-masked-text'
import * as Yup from 'yup'
import { TextField } from 'react-native-material-textfield'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import cpfValidation from 'cpf'

import { colors } from '../../styles'

const SigninSchema = Yup.object().shape({
  cpf: Yup.string()
    .required('Obrigatório')
    .test('cpf-is-valid', 'CPF Inválido', value => cpfValidation.isValid(value)),
  email: Yup.string()
    .email('E-mail Inválido')
    .required('Obrigatório'),
  fullName: Yup.string()
    .matches(/[\w]+([\s]+[\w]+)+/, 'Insira Seu Nome Completo')
    .required('Obrigatório'),
  birthDate: Yup.string()
})

class StepOne extends Component {
  nextStep = (values) => {
    values.birthDate = moment(values.birthDate, 'DD/MM/YYYY').format()
    this.props.navigation.navigate('StepPassword', { user: values })
  }

  render () {
    return (
      <View behavior='padding' style={styles.container}>
        <StatusBar backgroundColor={colors.primary} barStyle={'light-content'} />
        <View style={{ paddingHorizontal: 30, flex: 1, alignItems: 'flex-start', justifyContent: 'center' }}>
          <H2 style={{ color: '#fff', paddingBottom: 0 }}>Dados Pessoais</H2>
          <P style={{ color: '#fff', paddingVertical: 0 }}>Vamos precisar de algumas Informações suas</P>
        </View>
        <KeyboardAvoidingView style={{ flex: 5 }}>
          <View style={styles.elementsContainer}>
            <Formik
              initialValues={{ cpf: '', fullName: '', email: '', birthDate: '' }}
              validationSchema={SigninSchema}
              onSubmit={this.nextStep}
            >
              {({
                errors,
                handleSubmit,
                setFieldTouched,
                setFieldValue,
                touched,
                values
              }) => (
                <Fragment>
                  <TextField
                    label='CPF'
                    placeholder='000.000.000-00'
                    returnKeyType='next'
                    keyboardType='numeric'
                    value={values.cpf}
                    onChangeText={value => setFieldValue('cpf', MaskService.toMask('cpf', value))}
                    onBlur={() => setFieldTouched('cpf')}
                    error={touched.cpf && errors.cpf}
                    renderAccessory={() => <MaterialCommunityIcons name='account-card-details-outline' size={20} />}
                    tintColor='#ff674c'
                  />
                  <TextField
                    label='Nome Completo'
                    returnKeyType='next'
                    keyboardType='default'
                    autoCapitalize='none'
                    autoCorrect={false}
                    value={values.fullName}
                    onChangeText={value => setFieldValue('fullName', value)}
                    onBlur={() => setFieldTouched('fullName')}
                    error={touched.fullName && errors.fullName}
                    renderAccessory={() => <MaterialCommunityIcons name='account-circle-outline' size={20} />}
                    tintColor='#ff674c'
                  />
                  <TextField
                    label='E-mail'
                    returnKeyType='next'
                    keyboardType='email-address'
                    autoCapitalize='none'
                    autoCorrect={false}
                    value={values.email}
                    onChangeText={value => setFieldValue('email', value)}
                    onBlur={() => setFieldTouched('email')}
                    error={touched.email && errors.email}
                    renderAccessory={() => <MaterialCommunityIcons name='at' size={20} />}
                    tintColor='#ff674c'
                  />
                  <TextField
                    label='Data de nascimento'
                    placeholder='00/00/0000'
                    returnKeyType='done'
                    keyboardType='numeric'
                    value={values.birthDate}
                    onChangeText={value => setFieldValue('birthDate', MaskService.toMask('datetime', value))}
                    onBlur={() => setFieldTouched('birthDate')}
                    error={touched.birthDate && errors.birthDate}
                    renderAccessory={() => <MaterialCommunityIcons name='calendar-outline' size={20} />}
                    tintColor='#ff674c'
                  />
                  <View style={styles.buttonContainer}>
                    <Button
                      uppercase={false}
                      kind='rounded'
                      onPressIn={handleSubmit}>
                      Próximo
                    </Button>
                  </View>
                </Fragment>
              )}
            </Formik>
          </View>
        </KeyboardAvoidingView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: colors.primary
  },
  logoContainer: {
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo: {
    alignSelf: 'center',
    height: 350,
    width: '100%'
  },
  title: {
    color: '#FFFFFF',
    marginTop: 10,
    width: 160,
    textAlign: 'center',
    opacity: 0.9
  },
  elementsContainer: {
    flex: 1,
    backgroundColor: '#FFF',
    justifyContent: 'flex-start',
    padding: 30,
    borderTopRightRadius: 7,
    borderTopLeftRadius: 7
  },
  signupText: {
    color: 'rgba(87,83,102,0.6)',
    fontSize: 16,
    textAlign: 'center'
  },
  signupButton: {
    color: colors.secondary,
    fontSize: 16,
    fontWeight: '500'
  },
  buttonContainer: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row'
  }
})

export default StepOne
