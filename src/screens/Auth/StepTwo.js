import React, { Component, Fragment } from 'react'
import {
  KeyboardAvoidingView,
  StatusBar,
  StyleSheet,
  ScrollView,
  View,
  Image,
  TouchableOpacity
} from 'react-native'
import { Formik } from 'formik'
import { Button, H2, P } from 'nachos-ui'
import * as Yup from 'yup'
import { TextField } from 'react-native-material-textfield'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { debounce, each } from 'lodash'

import { getPlaceDetails, getPredictions } from '../../services/address'
import { colors } from '../../styles'
import PoweredByGoogle from '../../assets/images/powered_by_google_on_white_hdpi.png'

const AddressSchema = Yup.object().shape({
  email: Yup.string()
    .email('E-mail Inválido')
    .required('Obrigatório'),
  password: Yup.string()
    .min(4, 'Deve conter no mínimo 4 caracteres')
    .required('Obrigatório')
})

class StepTwo extends Component {
  constructor (props) {
    super(props)
    this.state = {
      predictions: [],
      selectedPlaceId: null
    }
  }

  nextStep = async () => {
    try {
      const { selectedPlaceId } = this.state
      const { navigation } = this.props
      let student = navigation.getParam('user')
      const { result } = await getPlaceDetails(selectedPlaceId)
      const { address_components: addr, geometry: { location: { lat, lng } } } = result;
      let components = {}
      each(addr, function (k, v1) {
        each(addr[v1].types, function (k2, v2) {
          components[addr[v1].types[v2]] = addr[v1].long_name
        })
      })
      const address = {
        publicPlace: components.route,
        neighborhood: components.sublocality,
        city: components.administrative_area_level_2,
        streetNumber: components.street_number,
        zipCode: components.postal_code,
        complement: '',
        latitude: lat,
        longitude: lng
      }
      student = Object.assign(student, { address })
      navigation.navigate('StepThree', { user: student })
    } catch (e) {
      console.error(e)
    }
  };

  onChangeAddress = debounce(async (value) => {
    try {
      const data = await getPredictions(value)
      const { predictions } = data
      this.setState({ predictions })
    } catch (e) {
      console.error(e)
    }
  }, 200);

  render () {
    const { predictions } = this.state
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor={colors.primary} barStyle={'light-content'} />
        <View style={{ paddingHorizontal: 30, flex: 1, alignItems: 'flex-start', justifyContent: 'center' }}>
          <H2 style={{ color: '#fff', paddingBottom: 0 }}>Endereço</H2>
          <P style={{ color: '#fff', paddingVertical: 0 }}>Digite seu endereço completo incluindo o número no campo abaixo.</P>
        </View>
        <KeyboardAvoidingView style={{ flex: 5 }}>
          <ScrollView contentContainerStyle={styles.elementsContainer}>
            <Formik
              initialValues={{ address: '' }}
              // validationSchema={SigninSchema}
              onSubmit={this.nextStep}
            >
              {({
                errors,
                handleSubmit,
                setFieldTouched,
                setFieldValue,
                values
              }) => (
                <Fragment>
                  <TextField
                    label='Endereço'
                    returnKeyType='done'
                    autoCorrect={false}
                    value={values.address}
                    onChangeText={(value) => {
                      setFieldValue('address', value)
                      this.onChangeAddress(value)
                    }}
                    onBlur={() => setFieldTouched('address')}
                    error={errors.address}
                    renderAccessory={() => <MaterialCommunityIcons name='mailbox-up-outline' size={20} />}
                    tintColor='#ff674c'
                  />
                  <View>
                    {predictions.map(({ place_id: placeid, description, structured_formatting: {
                      main_text: mainText,
                      secondary_text: secondaryText
                    } }, index) => (
                      <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} key={index} onPress={() => {
                        this.setState({ predictions: [], selectedPlaceId: placeid })
                        setFieldValue('address', description)
                      }}>
                        <MaterialCommunityIcons name='home-city-outline' size={20} />
                        <View style={{ marginLeft: 10 }}>
                          <P style={{ paddingBottom: 0 }}>{mainText}</P>
                          <P style={{ paddingTop: 0, fontSize: 9 }}>{secondaryText}</P>
                        </View>
                      </TouchableOpacity>
                    ))}
                    <View>
                      {predictions.length > 0 && (
                        <Image style={{ width: 100, resizeMode: 'contain' }} source={PoweredByGoogle} />
                      )}
                    </View>
                  </View>
                  <View style={styles.buttonContainer}>
                    <Button
                      uppercase
                      kind='rounded'
                      onPressIn={handleSubmit}
                      disabled={!values.address}>
                      Cadastrar Endereço
                    </Button>
                  </View>
                </Fragment>
              )}
            </Formik>
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: colors.primary
  },
  logoContainer: {
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo: {
    alignSelf: 'center',
    height: 350,
    width: '100%'
  },
  title: {
    color: '#FFFFFF',
    marginTop: 10,
    width: 160,
    textAlign: 'center',
    opacity: 0.9
  },
  elementsContainer: {
    flex: 1,
    backgroundColor: '#FFF',
    justifyContent: 'flex-start',
    padding: 30,
    borderTopRightRadius: 7,
    borderTopLeftRadius: 7
  },
  signupText: {
    color: 'rgba(87,83,102,0.6)',
    fontSize: 16,
    textAlign: 'center'
  },
  signupButton: {
    color: colors.secondary,
    fontSize: 16,
    fontWeight: '500'
  },
  buttonContainer: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row'
  }
})

export default StepTwo
