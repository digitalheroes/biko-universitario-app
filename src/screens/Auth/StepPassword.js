import React, { Component, Fragment } from 'react'
import {
  KeyboardAvoidingView,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
  View
} from 'react-native'
import { Formik } from 'formik'
import { showMessage } from 'react-native-flash-message'
import { Button, H2, P } from 'nachos-ui'
import * as Yup from 'yup'
import { TextField } from 'react-native-material-textfield'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

import { getCurrentStudent } from '../../services/students'
import { colors } from '../../styles'
import { createStudent } from '../../services/api'
import { setToken, setUser } from '../../helpers'

const SigninSchema = Yup.object().shape({
  password: Yup.string()
    .min(8, 'A senha deve conter 8 caracteres')
    .matches(/(?=.*[A-Za-z@$!%*#?&])(?=.*\d)[A-Za-z\d@$!%*#?&]{8,}/, 'A Senha deve conter letras e números')
    .required('Obrigatório')
})

class StepPassword extends Component {
  constructor (props) {
    super(props)
    this.state = {
      passwordVisible: false
    }
  }

  nextStep = (values) => {
    const { navigation } = this.props
    let student = navigation.getParam('user')
    let firstName = student.fullName.split(' ')[0]
    let lastName = student.fullName.split(' ')[1]
    student = Object.assign(student, values, { firstName }, { lastName })
    navigation.navigate('StepTwo', { user: student })
  };

  render () {
    const { passwordVisible } = this.state
    return (
      <View behavior='padding' style={styles.container}>
        <StatusBar backgroundColor={colors.primary} barStyle={'light-content'} />
        <View style={{ paddingHorizontal: 30, flex: 1, alignItems: 'flex-start', justifyContent: 'center' }}>
          <H2 style={{ color: '#fff', paddingBottom: 0 }}>Vamos proteger sua conta</H2>
          <P style={{ color: '#fff', paddingVertical: 0 }}>Digite uma senha segura abaixo, sua senha precisa conter letras e numeros.</P>
        </View>
        <KeyboardAvoidingView style={{ flex: 5 }} behavior='height'>
          <View style={styles.elementsContainer}>
            <Formik
              initialValues={{ password: '' }}
              validationSchema={SigninSchema}
              onSubmit={this.nextStep}
            >
              {({
                errors,
                handleSubmit,
                setFieldTouched,
                setFieldValue,
                values
              }) => (
                <Fragment>
                  <TextField
                    label='Digite uma senha'
                    returnKeyType='next'
                    keyboardType='default'
                    secureTextEntry={!passwordVisible}
                    value={values.password}
                    onChangeText={value => setFieldValue('password', value)}
                    onBlur={() => setFieldTouched('password')}
                    error={errors.password}
                    renderAccessory={() => (
                      <TouchableOpacity onPress={() => { this.setState({ passwordVisible: !passwordVisible }) }}>
                        <MaterialCommunityIcons name={passwordVisible ? 'eye-outline' : 'eye-off-outline'} size={20} />
                      </TouchableOpacity>
                    )}
                    tintColor='#ff674c'
                  />
                  <View style={styles.buttonContainer}>
                    <Button
                      uppercase={false}
                      kind='rounded'
                      onPressIn={handleSubmit}>
                      CADASTRAR
                    </Button>
                  </View>
                </Fragment>
              )}
            </Formik>
          </View>
        </KeyboardAvoidingView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: colors.primary
  },
  logoContainer: {
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo: {
    alignSelf: 'center',
    height: 350,
    width: '100%'
  },
  title: {
    color: '#FFFFFF',
    marginTop: 10,
    width: 160,
    textAlign: 'center',
    opacity: 0.9
  },
  elementsContainer: {
    flex: 1,
    backgroundColor: '#FFF',
    justifyContent: 'flex-start',
    padding: 30,
    borderTopRightRadius: 7,
    borderTopLeftRadius: 7
  },
  signupText: {
    color: 'rgba(87,83,102,0.6)',
    fontSize: 16,
    textAlign: 'center'
  },
  signupButton: {
    color: colors.secondary,
    fontSize: 16,
    fontWeight: '500'
  },
  buttonContainer: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row'
  }
})

export default StepPassword
