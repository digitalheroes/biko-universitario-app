import React, { Component, Fragment } from 'react'
import {
  KeyboardAvoidingView,
  StatusBar,
  StyleSheet,
  ScrollView,
  View,
  TouchableOpacity,
  FlatList
} from 'react-native'
import { Formik } from 'formik'
import { Button, H2, P } from 'nachos-ui'
import { showMessage } from 'react-native-flash-message'
import * as Yup from 'yup'
import { TextField } from 'react-native-material-textfield'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import TagHability from '../../components/Habilities/TagHability'
import { setToken, setUser } from '../../helpers'
import { getCurrentStudent } from '../../services/students'

import { colors } from '../../styles'
import { createStudent, searchHabilities } from '../../services/api'

class StepThree extends Component {
  constructor (props) {
    super(props)
    this.state = {
      habilities: [],
      selectedHabilities: []
    }
  }

  async fetchHabilities (text) {
    if (!text) {
      this.setState(prevState => ({
        ...prevState,
        habilities: []
      }))
    } else {
      try {
        let habilities = await searchHabilities(text)
        habilities = habilities.filter(
          hability =>
            !this.state.selectedHabilities.some(selectedHability =>
              selectedHability.id === hability.id
            )
        )
        this.setState({ habilities })
      } catch (e) {
        throw e
      }
    }
  }

  selectHability (habilityToSelect) {
    this.setState(prevState => ({
      selectedHabilities: [...prevState.selectedHabilities, habilityToSelect],
      habilities: prevState.habilities.filter(hability => hability.id !== habilityToSelect.id)
    }))
  }

  deselectHability (habilityToDeselect) {
    this.setState(prevState => ({
      selectedHabilities: prevState.selectedHabilities.filter(hability => hability.id !== habilityToDeselect.id)
    }))
  }

  updateHabilities = async () => {
    const { selectedHabilities } = this.state
    const { user, updateUser, navigation } = this.props
    const habilities = selectedHabilities.map(hability => hability.id)
    await updateUser({ habilities })
    navigation.pop()
  }

  findHability (query) {
    if (query === '') {
      return []
    }

    const { habilities } = this.state
    const regex = new RegExp(`${query.trim()}`, 'i')
    return habilities.filter(hability => hability.search(regex) >= 0)
  }

  signUp = async (values, { setSubmitting }) => {
    try {
      const { navigation } = this.props
      let student = navigation.getParam('user')
      const { selectedHabilities } = this.state
      const habilities = selectedHabilities.map(hability => hability.id)
      Object.assign(student, { habilities })
      const { token } = await createStudent(student)
      setToken(token)
      const user = await getCurrentStudent()
      setUser(user)
      showMessage({
        message: `Bem vindo`,
        description: 'Você foi cadastrado com sucesso',
        type: 'success',
        icon: 'success',
        backgroundColor: '#3cce71',
        floating: true
      })
      navigation.navigate('Jobs', { user: student })
    } catch (e) {
      showMessage({
        message: 'Erro',
        description: e.message,
        type: 'danger',
        icon: 'error',
        backgroundColor: '#f14e4e',
        floating: true
      })
    } finally {
      setSubmitting(false)
    }
  };

  render () {
    const { habilities, selectedHabilities } = this.state
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor={colors.primary} barStyle={'light-content'} />
        <View style={{ paddingHorizontal: 30, flex: 1, alignItems: 'flex-start', justifyContent: 'center' }}>
          <H2 style={{ color: '#fff', fontFamily: 'Palanquin', paddingBottom: 0 }}>Habilidades</H2>
          <P style={{ color: '#fff', paddingVertical: 0 }}>Preencha o campo e selecione suas habilidades.</P>
        </View>
        <KeyboardAvoidingView behavior='none' style={{ flex: 5 }}>
          <ScrollView contentContainerStyle={styles.elementsContainer}>
            <Formik
              initialValues={{ hability: '' }}
              onSubmit={this.signUp}
            >
              {({
                errors,
                handleSubmit,
                setFieldTouched,
                setFieldValue,
                values
              }) => (
                <Fragment>
                  <TextField
                    label='Pesquise por uma habilidade'
                    returnKeyType='done'
                    autoCorrect={false}
                    value={values.hability}
                    onChangeText={async value => {
                      setFieldValue('hability', value)
                      this.fetchHabilities(value)
                    }}
                    onBlur={() => setFieldTouched('hability')}
                    error={errors.hability}
                    renderAccessory={() => <MaterialCommunityIcons name='brain' size={20} />}
                    tintColor='#ff674c'
                  />
                  <FlatList
                    data={habilities}
                    renderItem={habilities =>
                      <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => this.selectHability(habilities.item)}>
                        <MaterialCommunityIcons name='brain' size={20} />
                        <View style={{ marginLeft: 10 }}>
                          <P>{habilities.item.habilityName}</P>
                        </View>
                      </TouchableOpacity>}
                    keyExtractor={(item, index) => index.toString()}
                  />
                  <View style={styles.selectedHabilities}>
                    {selectedHabilities.map(hability =>
                      <TagHability
                        key={hability.id}
                        hability={hability}
                        onPress={() => this.deselectHability(hability)} />
                    )}
                  </View>
                  <View style={styles.buttonContainer}>
                    <Button
                      uppercase={false}
                      kind='rounded'
                      onPress={handleSubmit}>
                      CADASTRAR HABILIDADES E FINALIZAR
                    </Button>
                  </View>
                </Fragment>
              )}
            </Formik>
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: colors.primary
  },
  logoContainer: {
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo: {
    alignSelf: 'center',
    height: 350,
    width: '100%'
  },
  title: {
    color: '#FFFFFF',
    marginTop: 10,
    width: 160,
    textAlign: 'center',
    opacity: 0.9
  },
  elementsContainer: {
    flex: 1,
    backgroundColor: '#FFF',
    justifyContent: 'flex-start',
    padding: 30,
    borderTopRightRadius: 7,
    borderTopLeftRadius: 7
  },
  signupText: {
    color: 'rgba(87,83,102,0.6)',
    fontSize: 16,
    textAlign: 'center'
  },
  signupButton: {
    color: colors.secondary,
    fontSize: 16,
    fontWeight: '500'
  },
  buttonContainer: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row'
  },
  selectedHabilities: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  }
})

export default StepThree
