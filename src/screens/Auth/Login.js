import React, { Component, Fragment } from 'react'
import {
  KeyboardAvoidingView,
  StatusBar,
  StyleSheet,
  Text,
  ScrollView,
  TouchableOpacity,
  View,
  SafeAreaView,
  Image
} from 'react-native'
import { Formik } from 'formik'
import { showMessage } from 'react-native-flash-message'
import { Button } from 'nachos-ui'
import * as Yup from 'yup'
import { TextField } from 'react-native-material-textfield'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'

import { getCurrentStudent } from '../../services/students'
import { colors } from '../../styles'
import { login } from '../../services/api'
import { setToken, setUser } from '../../helpers'

const SigninSchema = Yup.object().shape({
  email: Yup.string()
    .email('E-mail Inválido')
    .required('Obrigatório'),
  password: Yup.string()
    .min(4, 'Deve conter no mínimo 4 caracteres')
    .required('Obrigatório')
})

class Login extends Component {
  signIn = async (credentials, { setSubmitting }) => {
    try {
      const { token } = await login(credentials)
      setToken(token)
      const student = await getCurrentStudent()
      setUser(student)
      showMessage({
        message: `Bem vindo`,
        description: 'Você foi logado com sucesso',
        type: 'success',
        icon: 'success',
        backgroundColor: '#3cce71',
        floating: true
      })
      this.props.navigation.navigate('MainTabNavigator')
    } catch (e) {
      showMessage({
        message: 'Erro',
        description: e.message,
        type: 'danger',
        icon: 'error',
        backgroundColor: '#f14e4e',
        floating: true
      })
    } finally {
      setSubmitting(false)
    }
  };

  SignUp = () => {
    this.props.navigation.navigate('SignUp')
  };

  render () {
    return (
      <View behavior='padding' style={styles.container}>
        <StatusBar backgroundColor={colors.primary} barStyle={'light-content'} />
        <ScrollView>
          <SafeAreaView>
            <View style={styles.logoContainer}>
              <Image style={styles.logo} source={require('../../assets/images/login.png')} />
            </View>
          </SafeAreaView>
        </ScrollView>
        <KeyboardAvoidingView behavior='padding'>
          <View style={styles.elementsContainer}>
            <Formik
              initialValues={{ email: '', password: '' }}
              validationSchema={SigninSchema}
              onSubmit={this.signIn}
            >
              {({
                errors,
                handleSubmit,
                setFieldTouched,
                setFieldValue,
                values,
                touched
              }) => (
                <Fragment>
                  <TextField
                    label='Seu E-mail'
                    placeholder='nome@dominio.com'
                    returnKeyType='next'
                    keyboardType='email-address'
                    autoCapitalize='none'
                    autoCorrect={false}
                    value={values.email}
                    onChangeText={value => setFieldValue('email', value)}
                    onBlur={() => setFieldTouched('email')}
                    error={touched.email && errors.email}
                    renderAccessory={() => <MaterialCommunityIcons name='at' size={20} />}
                    tintColor='#ff674c'
                  />
                  <TextField
                    label='Digite sua senha'
                    returnKeyType='next'
                    keyboardType='default'
                    autoCapitalize='none'
                    secureTextEntry
                    autoCorrect={false}
                    value={values.password}
                    onChangeText={value => setFieldValue('password', value)}
                    onBlur={() => setFieldTouched('password')}
                    error={touched.password && errors.password}
                    renderAccessory={() => <MaterialIcons name='lock-outline' size={20} />}
                    tintColor='#ff674c'
                  />
                  <View style={styles.buttonContainer}>
                    <Button
                      uppercase={false}
                      kind='rounded'
                      onPressIn={handleSubmit}>
                      Login
                    </Button>
                  </View>
                </Fragment>
              )}
            </Formik>
            <View style={{ flexDirection: 'row', alignItems: 'center', alignContent: 'center', marginVertical: 20 }}>
              <Text style={styles.signupText}>Não tem uma conta ainda ? </Text>
              <TouchableOpacity onPress={this.SignUp}>
                <Text style={styles.signupButton}>Clique aqui</Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAvoidingView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: colors.primary
  },
  logoContainer: {
    position: 'relative',
    alignItems: 'center',
    flexGrow: 3,
    justifyContent: 'center'
  },
  logo: {
    alignSelf: 'center',
    height: 350,
    width: '100%'
  },
  title: {
    color: '#FFFFFF',
    marginTop: 10,
    width: 160,
    textAlign: 'center',
    opacity: 0.9
  },
  elementsContainer: {
    flexDirection: 'column',
    backgroundColor: '#FFF',
    justifyContent: 'flex-start',
    padding: 30,
    borderTopRightRadius: 7,
    borderTopLeftRadius: 7
  },
  signupText: {
    color: 'rgba(87,83,102,0.6)',
    fontSize: 16,
    textAlign: 'center'
  },
  signupButton: {
    color: colors.secondary,
    fontSize: 16,
    fontWeight: '500'
  },
  buttonContainer: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row'
  }
})

export default Login
