import React from 'react'
import { showMessage } from 'react-native-flash-message'

import Loading from '../../components/Loading/Loading'
import { getToken, setUser } from '../../helpers'
import { getCurrentStudent } from '../../services/students'

class AuthLoadingScreen extends React.Component {
  constructor (props) {
    super(props)
    this.fetchToken()
  }
    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    fetchToken = async () => {
      try {
        const token = await getToken()
        const { navigation } = this.props
        navigation.navigate(token ? 'MainTabNavigator' : 'AuthStack')
      } catch (e) {
        showMessage({
          message: 'Ops, aconteceu um erro:',
          description: e.message,
          type: 'danger',
          icon: 'danger'
        })
      }
    };
    // Render any loading content that you like here
    render () {
      return (
        <Loading />
      )
    }
}

export default AuthLoadingScreen
