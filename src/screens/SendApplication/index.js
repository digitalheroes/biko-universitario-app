import { withFormik } from 'formik'
import React, { Component } from 'react'
import { KeyboardAvoidingView, View } from 'react-native'
import { Input } from 'nachos-ui'
import { connect } from 'react-redux'
import RightButton from '../../components/RightButton'
import { createNewApplication } from '../../modules/applications'

class SendApplication extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Enviar Candidatura',
      headerRight: (
        <RightButton
          title={'Enviar'}
          onPress={navigation.state.params.sendApplication}
          loading={navigation.state.params.loading}
        />
      )
    }
  }

  constructor (props) {
    super(props)
    const { navigation, loading } = props
    navigation.setParams({ sendApplication: this.createApplication, loading })
  }

  createApplication = () => {
    const { navigation, values, createNewApplication } = this.props
    const job = navigation.getParam('job')
    const { id: JobId } = job
    const { applicationMessage } = values
    createNewApplication({ JobId, applicationMessage })
    navigation.pop()
  }

  componentDidUpdate (prevProps) {
    const { loading, navigation } = this.props
    if (prevProps.loading !== loading) {
      navigation.setParams({ loading })
    }
  }

  render () {
    const { values, setFieldValue, loading } = this.props
    return (
      <View>
        <KeyboardAvoidingView behavior='padding'>
          <Input
            value={values.applicationMessage}
            onChangeText={value => setFieldValue('applicationMessage', value)}
            style={{ height: 100, alignItems: 'flex-start', justifyContent: 'flex-start' }}
            inputStyle={{ height: 100, alignItems: 'flex-start', justifyContent: 'flex-start' }}
            placeholder='Envie um nota para a empresa, exemplo: texto de apresentação.'
            multiline
            numberOfLines={4}
            disabled={loading}
          />
        </KeyboardAvoidingView>
      </View>
    )
  }
}

const mapStateToProps = state => state.applications

const mapActionsToProps = {
  createNewApplication
}

export default connect(mapStateToProps, mapActionsToProps)(
  withFormik({
    mapPropsToValues: () => ({
      applicationMessage: ''
    })
  })(SendApplication)
)
