import PropTypes from 'prop-types'
import React from 'react'
import {
  Image,
  ScrollView,
  View,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
  Animated,
  StatusBar
} from 'react-native'
import ExtraDimensions from 'react-native-extra-dimensions-android'
import Feather from 'react-native-vector-icons/Feather'
import { connect } from 'react-redux'
import MapView from 'react-native-maps'
import { H1, P } from 'nachos-ui'
import Geocoder from 'react-native-geocoding'
import NotFoundIllustration from '../../assets/images/not-found.png'

import CurrentLocationMarker from '../../components/CurrentLocationMarker'
import JobCard from '../../components/JobCard'
import Colors from '../../constants/Colors'
import { setFilters, fetchJobs } from '../../modules/campings'

import jobImage from '../../assets/images/pin_image.png'
import job from '../../PropTypes/job'

import styles from './styles'
import mapStyle from '../../assets/mapStyle'

const { Marker } = MapView
let { width, height } = Dimensions.get('screen')
height = height - ExtraDimensions.getSoftMenuBarHeight()

class Jobs extends React.Component {
  static navigationOptions = {
    header: null
  };

  static propTypes = {
    fetchJobs: PropTypes.func.isRequired,
    jobs: PropTypes.arrayOf(job)
  }

  constructor (props) {
    super(props)
    const { fetchJobs } = this.props
    fetchJobs()
    this.state = {
      initialRegion: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0,
        longitudeDelta: 0
      }
    }
    this.animation = new Animated.Value(0)
    this.props.navigation.addListener(
      'willBlur',
      () => {
        StatusBar.setTranslucent(false)
        StatusBar.setBackgroundColor('#ffffff')
        StatusBar.setBarStyle('dark-content')
      }
    )
    this.props.navigation.addListener(
      'willFocus',
      () => {
        StatusBar.setTranslucent(true)
        StatusBar.setBackgroundColor('transparent')
        StatusBar.setBarStyle('dark-content')
      }
    )
    navigator.geolocation.watchPosition(
      async ({ coords }) => {
        this.setCurrentPlace(coords)
      },
      error => console.error(error)
    )
  }

  componentDidUpdate (prevProps, prevState, snapshot) {
    const { jobs } = this.props
    // We should detect when scrolling has stopped then animate
    // We should just debounce the event listener here
    if (jobs.length) {
      this.animation.addListener(({ value }) => {
        let index = Math.floor(value / 350 + 0.3) // animate 30% away from landing on the next item
        if (index >= jobs.length) {
          index = jobs.length - 1
        }
        if (index <= 0) {
          index = 0
        }

        const { Company: { Address: { latitude, longitude } } } = jobs[index]

        this.map.animateToRegion(
          {
            latitude: parseFloat(latitude),
            longitude: parseFloat(longitude),
            latitudeDelta: 0.05,
            longitudeDelta: 0.04
          },
          350
        )
      })
    }
  }

  componentWillUnmount () {
    this.animation.removeAllListeners()
  }

  openDrawer = () => {
    const { navigation } = this.props
    navigation.openDrawer()
  }

  setCurrentPlace = async (coords) => {
    const placeDetails = await Geocoder.from(coords)
    const { results } = placeDetails
    const [firstResult] = results
    const { address_components: address } = firstResult
    const [streetNumber, route] = address
    const { short_name: number } = streetNumber
    const { short_name: street } = route
    const currentLocation = `${street}, ${number}`
    const deltas = await this.calcDeltas(coords)
    this.setState({ currentLocation, initialRegion: { ...coords, ...deltas } })
    this.map.animateToRegion({ ...coords, ...deltas }, 2000)
  }

  calcDeltas = async ({ latitude, accuracy }) => {
    const oneDegreeOfLatitudeInMeters = 111.32 * 1000
    return {
      latitudeDelta: accuracy / oneDegreeOfLatitudeInMeters,
      longitudeDelta: accuracy / (oneDegreeOfLatitudeInMeters * Math.cos(latitude * (Math.PI / 180)))
    }
  }

  renderMap () {
    const { jobs } = this.props
    const { initialRegion } = this.state
    const { latitude: currentLat, longitude: currentLong } = initialRegion

    const interpolations = jobs.map((marker, index) => {
      const inputRange = [
        (index - 1) * 350,
        index * 350,
        ((index + 1) * 350)
      ]
      const opacity = this.animation.interpolate({
        inputRange,
        outputRange: [0.35, 1, 0.35],
        extrapolate: 'clamp'
      })
      return { opacity }
    })

    return (
      <View style={[styles.map]}>
        <SafeAreaView style={styles.mapHeader}>
          <TouchableOpacity onPress={this.openDrawer}>
            <View style={styles.menuButton}>
              <Feather
                name='menu'
                size={26}
                color={Colors.tabIconSelected}
              />
            </View>
          </TouchableOpacity>
        </SafeAreaView>
        <MapView
          ref={map => { this.map = map }}
          style={{ height, width }}
          showsMyLocationButton
          initialRegion={initialRegion}
          customMapStyle={mapStyle}
        >
          <CurrentLocationMarker coordinate={{ latitude: currentLat, longitude: currentLong }} />
          {jobs.map(({ Company: { Address: { latitude, longitude } } }, idx) => {
            const opacityStyle = {
              opacity: interpolations[idx].opacity
            }
            return (
              <Marker coordinate={{ latitude: parseFloat(latitude), longitude: parseFloat(longitude) }} key={idx}>
                <Animated.View style={[opacityStyle]}>
                  <Image source={jobImage} style={[{ maxHeight: 46 }]} resizeMode='contain' />
                </Animated.View>
              </Marker>
            )
          }
          )}
        </MapView>
        {jobs.length > 0 ? (
          <SafeAreaView style={styles.jobsContainer}>
            <View style={{ paddingHorizontal: 20 }}>
              <H1>Vagas Recomendadas</H1>
            </View>
            <Animated.ScrollView
              horizontal
              scrollEventThrottle={1}
              showsHorizontalScrollIndicator={false}
              snapToInterval={320}
              onScroll={Animated.event(
                [
                  {
                    nativeEvent: {
                      contentOffset: {
                        x: this.animation
                      }
                    }
                  }
                ],
                { useNativeDriver: true }
              )}
            >
              {jobs.map((marker, index) => (
                <JobCard job={marker} navigation={this.props.navigation} key={index} />
              ))}
            </Animated.ScrollView>
          </SafeAreaView>
        ) : (
          <SafeAreaView style={styles.imageContainer}>
            <Image source={NotFoundIllustration} />
            <P>Não foram encontradas vagas para esta região.</P>
          </SafeAreaView>
        )}
      </View>
    )
  }

  render () {
    return (
      this.renderMap()
    )
  }
}

const moduleState = state => ({
  jobs: state.campings.jobs,
  filters: state.campings.filters
})

const moduleActions = {
  fetchJobs,
  setFilters
}

export default connect(moduleState, moduleActions)(Jobs)
