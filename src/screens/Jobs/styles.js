import { Dimensions, StyleSheet } from 'react-native'
import ExtraDimensions from 'react-native-extra-dimensions-android'
let { width, height } = Dimensions.get('screen')
height = height - ExtraDimensions.getSoftMenuBarHeight()

console.log(Dimensions.get('window'))
console.log(Dimensions.get('screen'))
console.log(ExtraDimensions.getRealWindowHeight())
console.log(ExtraDimensions.getSoftMenuBarHeight())
console.log(ExtraDimensions.get('SOFT_MENU_BAR_HEIGHT'))

export default StyleSheet.create({
  container: {
    backgroundColor: '#fff'
  },
  jobsContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    paddingVertical: 10,
    marginVertical: 10
  },
  cardImage: {
    flex: 3,
    width: '100%',
    height: '100%',
    alignSelf: 'center'
  },
  headerContainer: {
    top: 0,
    height: height * 0.15,
    width: width
  },
  header: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: height * 0.15,
    paddingHorizontal: 14
  },
  location: {
    height: 24,
    width: 24,
    borderRadius: 14,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ff7657'
  },
  marker: {
    width: 40,
    height: 40,
    borderRadius: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#FFF'
  },
  rvMarker: {
    backgroundColor: '#FFBA5A'
  },
  tentMarker: {
    backgroundColor: '#FF7657'
  },
  settings: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  options: {
    flex: 1,
    paddingHorizontal: 14
  },
  tabs: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  tab: {
    paddingHorizontal: 14,
    marginHorizontal: 10,
    borderBottomWidth: 3,
    borderBottomColor: 'transparent'
  },
  tabTitle: {
    fontWeight: 'bold',
    fontSize: 14,
    marginBottom: 10
  },
  activeTab: {
    borderBottomColor: '#FF7657'
  },
  activeTabTitle: {
    color: '#FF7657'
  },
  map: {
  },
  mapHeader: {
    flex: 1,
    position: 'absolute',
    zIndex: 2,
    top: 32,
    left: 16
  },
  menuButton: {
    paddingVertical: 10,
    paddingHorizontal: 12,
    backgroundColor: '#ffffff',
    borderRadius: 60
  },
  camping: {
    flex: 1,
    flexDirection: 'row',
    borderBottomColor: '#A5A5A5',
    borderBottomWidth: 0.5,
    padding: 20
  },
  campingDetails: {
    flex: 2,
    paddingLeft: 20,
    flexDirection: 'column',
    justifyContent: 'space-around'
  },
  campingInfo: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 14
  },
  campingImage: {
    width: width * 0.30,
    height: width * 0.25,
    borderRadius: 6
  },
  myMarker: {
    zIndex: 2,
    width: 60,
    height: 60,
    borderRadius: 60,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(51, 83, 251, 0.2)'
  },
  myMarkerDot: {
    width: 12,
    height: 12,
    borderRadius: 12,
    backgroundColor: '#3353FB'
  },
  imageContainer: {
    position: 'absolute',
    bottom: 20,
    left: 0,
    right: 0,
    paddingVertical: 10,
    marginVertical: 15,
    marginHorizontal: 20,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
