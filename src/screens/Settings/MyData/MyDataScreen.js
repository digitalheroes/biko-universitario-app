import moment from 'moment'
import React, { Component, Fragment } from 'react'
import {
  KeyboardAvoidingView,
  StyleSheet,
  View
} from 'react-native'
import { Formik } from 'formik'
import { MaskService } from 'react-native-masked-text'
import * as Yup from 'yup'
import { TextField } from 'react-native-material-textfield'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import cpfValidation from 'cpf'

import { colors } from '../../../styles'

const SigninSchema = Yup.object().shape({
  cpf: Yup.string()
    .required('Obrigatório')
    .test('cpf-is-valid', 'CPF Inválido', value => cpfValidation.isValid(value)),
  email: Yup.string()
    .email('E-mail Inválido')
    .required('Obrigatório'),
  fullName: Yup.string()
    .matches(/[\w]+([\s]+[\w]+)+/, 'Insira Seu Nome Completo')
    .required('Obrigatório'),
  birthDate: Yup.string()
    .required('Obrigatório')
})

class MyDataScreen extends Component {
  static navigationOptions = {
    title: 'Meu perfil'
  }

  constructor (props) {
    super(props)
    const user = props.navigation.getParam('user')
    this.state = { user }
  }
  render () {
    const { user } = this.state
    return (
      <View behavior='padding' style={styles.container}>
        <KeyboardAvoidingView style={{ flex: 5 }}>
          <View style={styles.elementsContainer}>
            <Formik
              initialValues={{ cpf: user.cpf, fullName: `${user.firstName} ${user.lastName}`, email: user.email, birthDate: user.birthDate }}
              validationSchema={SigninSchema}
              onSubmit={null}
            >
              {({
                errors,
                handleSubmit,
                setFieldTouched,
                setFieldValue,
                touched,
                values
              }) => (
                <Fragment>
                  <TextField
                    label='CPF'
                    disabled
                    placeholder='000.000.000-00'
                    returnKeyType='next'
                    keyboardType='numeric'
                    value={values.cpf}
                    onChangeText={value => setFieldValue('cpf', MaskService.toMask('cpf', value))}
                    onBlur={() => setFieldTouched('cpf')}
                    error={touched.cpf && errors.cpf}
                    renderAccessory={() => <MaterialCommunityIcons name='account-card-details-outline' size={20} />}
                    tintColor='#ff674c'
                  />
                  <TextField
                    label='Nome Completo'
                    returnKeyType='next'
                    keyboardType='default'
                    autoCapitalize='none'
                    disabled
                    autoCorrect={false}
                    value={values.fullName}
                    onChangeText={value => setFieldValue('fullName', value)}
                    onBlur={() => setFieldTouched('fullName')}
                    error={touched.fullName && errors.fullName}
                    renderAccessory={() => <MaterialCommunityIcons name='account-circle-outline' size={20} />}
                    tintColor='#ff674c'
                  />
                  <TextField
                    label='E-mail'
                    disabled
                    returnKeyType='next'
                    keyboardType='email-address'
                    autoCapitalize='none'
                    autoCorrect={false}
                    value={values.email}
                    onChangeText={value => setFieldValue('email', value)}
                    onBlur={() => setFieldTouched('email')}
                    error={touched.email && errors.email}
                    renderAccessory={() => <MaterialCommunityIcons name='at' size={20} />}
                    tintColor='#ff674c'
                  />
                  <TextField
                    disabled
                    label='Data de nascimento'
                    placeholder='00/00/0000'
                    returnKeyType='done'
                    keyboardType='numeric'
                    value={moment(values.birthDate).format('DD/MM/yyyy')}
                    onChangeText={value => setFieldValue('birthDate', moment.format(value, 'dd/MM/yyyy'))}
                    onBlur={() => setFieldTouched('birthDate')}
                    error={touched.birthDate && errors.birthDate}
                    renderAccessory={() => <MaterialCommunityIcons name='calendar-outline' size={20} />}
                    tintColor='#ff674c'
                  />
                </Fragment>
              )}
            </Formik>
          </View>
        </KeyboardAvoidingView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: colors.primary
  },
  logoContainer: {
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo: {
    alignSelf: 'center',
    height: 350,
    width: '100%'
  },
  title: {
    color: '#FFFFFF',
    marginTop: 10,
    width: 160,
    textAlign: 'center',
    opacity: 0.9
  },
  elementsContainer: {
    flex: 1,
    backgroundColor: '#FFF',
    justifyContent: 'flex-start',
    padding: 30
  },
  signupText: {
    color: 'rgba(87,83,102,0.6)',
    fontSize: 16,
    textAlign: 'center'
  },
  signupButton: {
    color: colors.secondary,
    fontSize: 16,
    fontWeight: '500'
  },
  buttonContainer: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row'
  }
})

export default MyDataScreen
