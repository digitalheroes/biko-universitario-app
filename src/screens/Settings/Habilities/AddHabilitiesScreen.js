import React, { Component } from 'react'
import { withFormik } from 'formik'
import {
  FlatList,
  KeyboardAvoidingView,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from 'react-native'
import { Input } from 'nachos-ui'
import { connect } from 'react-redux'

import RightButton from '../../../components/RightButton'
import { updateUser } from '../../../modules/user'
import colors from '../../../styles/colors'
import fonts from '../../../styles/fonts'
import { searchHabilities } from '../../../services/api'
import TagHability from '../../../components/Habilities/TagHability'

const styles = StyleSheet.create({
  selectedHabilities: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginHorizontal: 15
  },
  habilityText: {
    borderTopWidth: 1,
    borderTopColor: '#eee',
    marginHorizontal: 20,
    paddingVertical: 20,
    paddingHorizontal: 10,
    fontSize: fonts.big,
    color: colors.secondary,
    fontWeight: '700'
  },
  wrapper: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: '#fff'
  }
})

class AddHabilitiesScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Adicionar Habilidades',
      headerRight: (
        <RightButton
          title={'Salvar'}
          onPress={navigation.state.params.saveHabilities}
          loading={navigation.state.params.loading}
        />
      )
    }
  }

  constructor (props) {
    super(props)
    const { navigation, loading } = props
    navigation.setParams({ saveHabilities: this.updateHabilities, loading })
    this.state = {
      habilities: [],
      selectedHabilities: [],
      query: ''
    }
  }

  componentDidUpdate (prevProps) {
    const { loading, navigation } = this.props
    if (prevProps.loading !== loading) {
      navigation.setParams({ loading })
    }
  }

  async fetchHabilities (text) {
    if (!text) {
      this.setState(prevState => ({
        ...prevState,
        habilities: []
      }))
    } else {
      try {
        let habilities = await searchHabilities(text)
        habilities = habilities.filter(
          hability =>
            !this.state.selectedHabilities.some(selectedHability =>
              selectedHability.id === hability.id
            )
        )
        this.setState({ habilities })
      } catch (e) {
        throw e
      }
    }
  }

  selectHability (habilityToSelect) {
    this.setState(prevState => ({
      selectedHabilities: [...prevState.selectedHabilities, habilityToSelect],
      habilities: prevState.habilities.filter(hability => hability.id !== habilityToSelect.id)
    }))
  }

  deselectHability (habilityToDeselect) {
    this.setState(prevState => ({
      selectedHabilities: prevState.selectedHabilities.filter(hability => hability.id !== habilityToDeselect.id)
    }))
  }

  updateHabilities = async () => {
    const { selectedHabilities } = this.state
    const { user, updateUser, navigation } = this.props
    const habilities = selectedHabilities.map(hability => hability.id)
    await updateUser({ habilities })
    navigation.pop()
  }

  findHability (query) {
    if (query === '') {
      return []
    }

    const { habilities } = this.state
    const regex = new RegExp(`${query.trim()}`, 'i')
    return habilities.filter(hability => hability.search(regex) >= 0)
  }

  render () {
    const { values, setFieldValue } = this.props
    const { habilities, selectedHabilities } = this.state
    return (
      <View style={styles.wrapper}>
        <KeyboardAvoidingView behavior={'padding'}>
          <Input
            placeholder='Digite uma habilidade'
            value={values.hability}
            onChangeText={value => {
              setFieldValue('hability', value)
              this.fetchHabilities(value)
            }}
          />
          <FlatList
            data={habilities}
            renderItem={habilities =>
              <TouchableHighlight onPress={() => this.selectHability(habilities.item)}
                underlayColor={colors.primary}>
                <Text style={styles.habilityText}>{habilities.item.habilityName}</Text>
              </TouchableHighlight>}
            keyExtractor={(item, index) => index.toString()} />
          <View style={styles.selectedHabilities}>
            {selectedHabilities.map(hability =>
              <TagHability
                key={hability.id}
                hability={hability}
                onPress={() => this.deselectHability(hability)} />
            )}
          </View>
        </KeyboardAvoidingView>
      </View>
    )
  }
}

const mapStateToProps = state => state.user

const mapDispatchToProps = {
  updateUser
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withFormik({
    mapPropsToValues: () => ({
      hability: ''
    })
  })(AddHabilitiesScreen)
)
