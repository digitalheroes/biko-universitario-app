import React, { Component } from 'react'
import { Image, View, StyleSheet, ScrollView, Text } from 'react-native'
import { H3, P, Button } from 'nachos-ui'

import { getCurrentStudent } from '../../../services/students'
import fonts from '../../../styles/fonts'
import colors from '../../../styles/colors'
import NotFoundIllustration from '../../../assets/images/not-found.png'

const styles = StyleSheet.create({
  scrollWrapper: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#ffffff',
    paddingHorizontal: 20
  },
  subTitle: {
    marginVertical: 0,
    marginHorizontal: 15,
    fontSize: fonts.normal,
    color: colors.secondary
  },
  warningTitle: {
    marginVertical: 25,
    marginHorizontal: 25,
    fontSize: fonts.big,
    color: colors.secondary
  },
  imageContainer: {
    marginVertical: 15,
    marginHorizontal: 15,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

class HabilitiesScreenMain extends Component {
  static navigationOptions = {
    title: 'Habilidades'
  }

  constructor (props) {
    super(props)
    this.state = {
      user: {}
    }
    this.fetchUser()
  }

  componentDidUpdate () {
    this.fetchUser()
  }

  async fetchUser () {
    const user = await getCurrentStudent()
    this.setState({ user })
  }

  render () {
    const { navigation } = this.props
    const { user } = this.state
    const { Habilities: habilities } = user
    return (
      <ScrollView style={styles.scrollWrapper}>
        <H3>
          Habilidades são uma versão mais precisa da sua experiência
        </H3>
        <P>
          As habilidades ajudam os empregadores a entender sua combinação única de forças e o que você pode trazer para
          um trabalho
        </P>
        {habilities && habilities.length === 0
          ? (
            <View style={styles.imageContainer}>
              <Image source={NotFoundIllustration} />
              <P>Não existem habilidades cadastradas.</P>
            </View>
          ) : (
            habilities && habilities.map(({ habilityName }) => (
              <View>
                <Text>{habilityName}</Text>
              </View>
            ))
          )}
        <Button
          onPress={() => navigation.navigate('AddHabilitiesScreen', {})} >
          Adicionar habilidades
        </Button>
      </ScrollView>
    )
  }
}

export default HabilitiesScreenMain
