import React, { Component } from 'react'
import {
  View,
  ScrollView, TouchableWithoutFeedback
} from 'react-native'
import Feather from 'react-native-vector-icons/Feather'

import { clear, getUser } from '../../helpers'
import Row from '../../components/Common/Row/Row'

export const menuIcon = ({ tintColor, scene }: { tintColor: string, scene: Object }) => (
  <TouchableWithoutFeedback onPress={() => {
    const { descriptor } = scene
    const { navigation } = descriptor
    navigation.toggleDrawer()
  }}>
    <View style={{ paddingVertical: 10, paddingHorizontal: 12, marginHorizontal: 16 }}>
      <Feather name='menu' size={26} color={tintColor} />
    </View>
  </TouchableWithoutFeedback>
)

class SettingsScreen extends Component {
  static navigationOptions = {
    title: 'Configurações',
    headerLeft: menuIcon
  }

  constructor (props) {
    super(props)
    this.state = {
      user: {}
    }
    this.fetchUser()
  }

  fetchUser = async () => {
    const user = await getUser()
    this.setState({ user })
  }

  logout = async () => {
    await clear()
    this.props.navigation.navigate('AuthStack')
  };

  render () {
    const { user } = this.state
    return (
      <ScrollView style={{ backgroundColor: '#ffffff' }}>
        <View>
          <Row
            iconName='user'
            text='Meu perfil'
            onPress={() => this.props.navigation.navigate('MyDataScreen', { user })} />
          <Row
            iconName='clipboard'
            text='Candidaturas'
            onPress={() => this.props.navigation.navigate('ApplicationsScreen')} />
          <Row
            iconName='feather'
            text='Habilidades'
            onPress={() => this.props.navigation.navigate('HabilitiesScreen', { user })} />
          {/* <Row */}
          {/*  iconName='book' */}
          {/*  text='Educação' */}
          {/*  onPress={() => this.props.navigation.navigate('ApplicationsScreen')} /> */}
          {/* <Row */}
          {/*  iconName='help-circle' */}
          {/*  text='Ajuda' */}
          {/*  onPress={() => this.props.navigation.navigate('ApplicationsScreen')} /> */}
          <Row
            iconName='log-out'
            text='Sair'
            onPress={this.logout} />
        </View>
      </ScrollView>
    )
  }
}

export default SettingsScreen
