import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { ScrollView, Text } from 'react-native'
import { Card, H1 } from 'nachos-ui'
import { connect } from 'react-redux'
import moment from 'moment'
import 'moment/locale/pt-br'
import ApplicationListItem from '../../../components/ApplicationListItem'
import { getUser } from '../../../helpers'
import { getApplications } from '../../../modules/applications'

class ApplicationsScreenMain extends Component {
  static navigationOptions = {
    title: 'Candidaturas'
  }

  constructor (props) {
    super(props)
    this.state = {
      user: {}
    }
    this.fetchData()
  }

  async fetchData () {
    const user = await getUser()
    const { id } = user
    const { getApplications } = this.props
    this.setState({ user })
    getApplications(id)
  }

  render () {
    const { applications } = this.props
    return (
      <ScrollView>
        {applications.map((application, index) => {
          return (
            <ApplicationListItem key={index} application={application} />
          )
        })}
      </ScrollView>
    )
  }
}

const mapStateToProps = state => state.applications

const mapDispatchToProps = {
  getApplications
}

ApplicationsScreenMain.propTypes = {
  applications: PropTypes.array,
  getApplications: PropTypes.func
}

export default connect(mapStateToProps, mapDispatchToProps)(ApplicationsScreenMain)
