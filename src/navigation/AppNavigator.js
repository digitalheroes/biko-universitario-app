import { createAppContainer, createStackNavigator, createSwitchNavigator } from 'react-navigation'
import AuthLoadingScreen from '../screens/Auth/AuthLoadingScreen'
import Login from '../screens/Auth/Login'
import StepOne from '../screens/Auth/StepOne'
import StepPassword from '../screens/Auth/StepPassword'
import StepThree from '../screens/Auth/StepThree'
import StepTwo from '../screens/Auth/StepTwo'
import MainTabNavigator from './MainTabNavigator'

const AuthStack = createStackNavigator(
  {
    Login,
    SignUp: StepOne,
    StepTwo,
    StepThree,
    StepPassword
  },
  {
    defaultNavigationOptions: {
      header: null
    },
    headerLayoutPreset: 'left'
  }
)

export default createAppContainer(
  createSwitchNavigator(
    {
      AuthLoadingScreen,
      AuthStack,
      MainTabNavigator
    })
)
