import React, { useState, useEffect } from 'react'
import { ScrollView, SafeAreaView, View, ImageBackground } from 'react-native'
import { DrawerItems } from 'react-navigation'
import { H3, P } from 'nachos-ui'
import { connect } from 'react-redux'
import { getUser } from '../helpers'
import { colors } from '../styles'
import Cover from '../assets/images/cover.jpg'

const CustomDrawerComponent = ({ ...props }) => {
  useEffect(() => {
    fetchUser()
  }, [])
  const [user, setUser] = useState({})
  const fetchUser = async () => {
    const data = await getUser()
    console.log(data)
    setUser(data)
  }
  const { firstName, lastName, email } = user
  return (
    <View style={{ flex: 1 }}>
      <ImageBackground source={Cover} style={{ width: '100%' }}>
        <View style={{ opacity: 0.90, backgroundColor: 'rgba(245, 113, 79, 0.90)' }}>
          <SafeAreaView>
            <View style={{ paddingHorizontal: 20, paddingVertical: 40 }}>
              <H3 style={{ color: colors.white, paddingBottom: 0 }}>{firstName} {lastName}</H3>
              <P style={{ color: colors.white, paddingTop: 0 }}>{email}</P>
            </View>
          </SafeAreaView>
        </View>
      </ImageBackground>
      <ScrollView>
        <DrawerItems activeTintColor={colors.primary} {...props} />
      </ScrollView>
    </View>
  )
}

const mapStateToProps = state => state.user

export default connect(mapStateToProps)(CustomDrawerComponent)
