import React from 'react'
import { View } from 'react-native'
import AntDesign from 'react-native-vector-icons/AntDesign'

export const headerBackImage = ({ tintColor }: { tintColor: string }) => (
  <View style={{ paddingHorizontal: 20 }}>
    <AntDesign name='arrowleft' size={26} color={tintColor} />
  </View>
)

export const stackConfig = {
  defaultNavigationOptions: {
    headerStyle: {},
    headerTintColor: '#37455c',
    headerBackImage,
    headerBackTitle: null,
    headerBackTitleVisible: false
  },
  headerLayoutPreset: 'center'
}
