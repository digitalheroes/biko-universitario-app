import React from 'react'
import { createStackNavigator, createDrawerNavigator } from 'react-navigation'

import TabBarIcon from '../components/TabBarIcon'
import JobApplication from '../screens/JobDetail'
import Jobs from '../screens/Jobs'
import SendApplication from '../screens/SendApplication'
import ApplicationsScreen from '../screens/Settings/Applications/ApplicationsScreen'
import AddHabilitiesScreen from '../screens/Settings/Habilities/AddHabilitiesScreen'
import HabilitiesScreen from '../screens/Settings/Habilities/HabilitiesScreen'
import MyDataScreen from '../screens/Settings/MyData/MyDataScreen'
import SettingsScreen from '../screens/Settings/SettingsScreen'
import CustomDrawerComponent from './CustomDrawerComponent'
import { headerBackImage, stackConfig } from './stackConfig'

const HomeStack = createStackNavigator(
  {
    Jobs,
    JobApplication,
    SendApplication
  },
  stackConfig
)
HomeStack.navigationOptions = {
  drawerLabel: 'Vagas',
  drawerIcon: ({ focused }: { focused: boolean }) => (
    <TabBarIcon
      focused={focused}
      name='briefcase'
    />
  ),
  headerBackTitleVisible: false
}

const SettingsStack = createStackNavigator(
  {
    SettingsScreen,
    HabilitiesScreen,
    AddHabilitiesScreen,
    ApplicationsScreen,
    MyDataScreen
  },
  stackConfig
)
SettingsStack.navigationOptions = {
  drawerLabel: 'Configurações',
  drawerIcon: ({ focused }: { focused: boolean }) => (
    <TabBarIcon
      focused={focused}
      name='cog'
    />
  ),
  headerBackTitleVisible: false
}

export default createDrawerNavigator(
  {
    HomeStack,
    SettingsStack
  },
  {
    contentComponent: CustomDrawerComponent,
    defaultNavigationOptions: {
      headerStyle: {},
      headerTintColor: '#37455c',
      headerBackImage,
      headerBackTitleVisible: false
    },
    headerLayoutPreset: 'center'
  },
  stackConfig,
)
