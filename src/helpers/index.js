import AsyncStorage from '@react-native-community/async-storage'
import { STORAGE_TOKEN_KEY, STORAGE_USER_KEY } from '../constants'

/**
 * get token from phone localStorage
 */
export const getToken = () => AsyncStorage.getItem(STORAGE_TOKEN_KEY)

/**
 * Set token to phone localStorage
 * @param token - the token to be set
 */
export const setToken = token => AsyncStorage.setItem(STORAGE_TOKEN_KEY, token)

export const clearToken = () => AsyncStorage.removeItem(STORAGE_TOKEN_KEY)

/**
 * get token from phone localStorage
 */
export const getUser = async () => {
  const user = await AsyncStorage.getItem(STORAGE_USER_KEY)
  return JSON.parse(user)
}

/**
 * Set token to phone localStorage
 * @param user - the user to be set
 */
export const setUser = user =>
  AsyncStorage.setItem(STORAGE_USER_KEY, JSON.stringify(user))

export const clear = () => {
  AsyncStorage.clear()
}
