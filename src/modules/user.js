import { updateStudent } from '../services/students'

// Actions
const UPDATE_USER_REQUEST = 'UPDATE_USER_REQUEST'
const UPDATE_USER_SUCCESS = 'UPDATE_USER_SUCCESS'
const UPDATE_USER_FAILURE = 'UPDATE_USER_FAILURE'

// Initial state
const INITIAL_STATE = {
  user: {},
  loading: false,
  error: undefined
}

// Reducer
export default function reducer (state = INITIAL_STATE, action = {}) {
  switch (action.type) {
    case UPDATE_USER_REQUEST:
      return {
        ...state,
        loading: true
      }
    case UPDATE_USER_SUCCESS:
      return {
        ...state,
        user: action.payload,
        loading: false
      }
    case UPDATE_USER_FAILURE:
      return {
        ...state,
        error: action.payload,
        loading: false
      }
    default:
      return state
  }
};

export function updateUserRequest () {
  return {
    type: UPDATE_USER_REQUEST
  }
}

export function updateUserSuccess (payload) {
  return {
    type: UPDATE_USER_SUCCESS,
    payload
  }
}

export function updateUserFailure (payload) {
  return {
    type: UPDATE_USER_FAILURE,
    payload
  }
}

// Action Creators
export function updateUser (user) {
  return async (dispatch) => {
    dispatch(updateUserRequest())
    try {
      const updatedUser = await updateStudent(user)
      dispatch(updateUserSuccess(updatedUser))
    } catch (e) {
      dispatch(updateUserFailure(e))
    }
  }
}
