import { createApplication } from '../services/applications'
import {getStudentApplications} from '../services/students'

// Actions
const CREATE_APPLICATION_REQUEST = 'CREATE_APPLICATION_REQUEST'
const CREATE_APPLICATION_SUCCESS = 'CREATE_APPLICATION_SUCCESS'
const CREATE_APPLICATION_FAILURE = 'CREATE_APPLICATION_FAILURE'
const GET_APPLICATIONS_REQUEST = 'GET_APPLICATIONS_REQUEST'
const GET_APPLICATIONS_SUCCESS = 'GET_APPLICATIONS_SUCCESS'
const GET_APPLICATIONS_FAILURE = 'GET_APPLICATIONS_FAILURE'

// Initial state
const INITIAL_STATE = {
  application: {
    JobId: undefined,
    applicationMessage: ''
  },
  applications: [],
  loading: false,
  error: undefined
}

// Reducer
export default function reducer (state = INITIAL_STATE, action = {}) {
  console.log(action)
  switch (action.type) {
    case CREATE_APPLICATION_REQUEST:
      return {
        ...state,
        loading: true
      }
    case CREATE_APPLICATION_SUCCESS:
      return {
        ...state,
        application: action.payload,
        loading: false
      }
    case CREATE_APPLICATION_FAILURE:
      return {
        ...state,
        error: action.payload,
        loading: false
      }
    case GET_APPLICATIONS_REQUEST:
      return {
        ...state,
        loading: true
      }
    case GET_APPLICATIONS_SUCCESS:
      return {
        ...state,
        applications: action.payload,
        loading: false
      }
    case GET_APPLICATIONS_FAILURE:
      return {
        ...state,
        error: action.payload,
        loading: false
      }
    default:
      return state
  }
};

export function createApplicationRequest () {
  return {
    type: CREATE_APPLICATION_REQUEST
  }
}

export function createApplicationSuccess (payload) {
  return {
    type: CREATE_APPLICATION_SUCCESS,
    payload
  }
}

export function createApplicationFailure (payload) {
  return {
    type: CREATE_APPLICATION_FAILURE,
    payload
  }
}

export function getApplicationsRequest () {
  return {
    type: GET_APPLICATIONS_REQUEST
  }
}

export function getApplicationsSuccess (payload) {
  return {
    type: GET_APPLICATIONS_SUCCESS,
    payload
  }
}

export function getApplicationsFailure (payload) {
  return {
    type: GET_APPLICATIONS_FAILURE,
    payload
  }
}

// Action Creators
export function createNewApplication (application) {
  return async (dispatch) => {
    dispatch(createApplicationRequest())
    try {
      const createdApplication = await createApplication(application)
      dispatch(createApplicationSuccess(createdApplication))
    } catch (e) {
      dispatch(createApplicationFailure(e))
    }
  }
}

export function getApplications (studentId) {
  return async (dispatch) => {
    dispatch(getApplicationsRequest())
    try {
      const applications = await getStudentApplications(studentId)
      dispatch(getApplicationsSuccess(applications))
    } catch (e) {
      dispatch(getApplicationsFailure(e))
    }
  }
}
