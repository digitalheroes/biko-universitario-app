import {getJobById, getJobs} from '../services/jobs'

// Actions
const SET_JOBS_REQUEST = 'Spots/campings/SET_JOBS_REQUEST'
const SET_JOBS_SUCCESS = 'Spots/campings/SET_JOBS_SUCCESS'
const SET_JOBS_FAILURE = 'Spots/campings/SET_JOBS_FAILURE'
const SET_LOCATION = 'Spots/campings/SET_LOCATION'
const SET_FILTERS = 'Spots/campings/SET_FILTERS'
const SET_LOADING = 'Spots/campings/SET_LOADING'
const GET_JOB_DETAIL_REQUEST = 'GET_JOB_DETAIL_REQUEST'
const GET_JOB_DETAIL_SUCCESS = 'GET_JOB_DETAIL_SUCCESS'
const GET_JOB_DETAIL_FAILURE = 'GET_JOB_DETAIL_FAILURE'

// Initial state
const INITIAL_STATE = {
  jobs: [],
  jobDetail: undefined,
  mylocation: {
    latitude: 37.79035,
    longitude: -122.4384
  },
  filters: {
    sort: 'distance',
    type: 'all',
    price: 'free',
    option_full: true,
    option_rated: true,
    option_free: false
  },
  loading: false,
  error: undefined
}

// Reducer
export default function reducer (state = INITIAL_STATE, action = {}) {
  switch (action.type) {
    case SET_JOBS_SUCCESS:
      return {
        ...state,
        jobs: action.payload
      }
    case SET_JOBS_FAILURE:
      return {
        ...state,
        error: action.payload
      }
    case SET_LOCATION:
      return {
        ...state,
        location: action.payload
      }
    case SET_FILTERS:
      return {
        ...state,
        filters: {
          ...state.filters,
          ...action.payload
        }
      }
    case SET_LOADING:
      return {
        ...state,
        loading: action.payload
      }
    default:
      return state
  }
};

export function setJobsRequest () {
  return {
    type: SET_JOBS_REQUEST
  }
}

export function setJobsSuccess (payload) {
  return {
    type: SET_JOBS_SUCCESS,
    payload
  }
}

export function setJobsFailure (payload) {
  return {
    type: SET_JOBS_FAILURE,
    payload
  }
}

// Action Creators
export function fetchJobs () {
  return async (dispatch) => {
    dispatch(setJobsRequest())
    try {
      const jobs = await getJobs()
      dispatch(setJobsSuccess(jobs))
    } catch (e) {
      dispatch(setJobsFailure(e))
    }
  }
}

export function fetchJobDetail (id) {
  return async (dispatch) => {
    dispatch({
      type: GET_JOB_DETAIL_REQUEST
    })
    try {
      const jobDetail = await getJobById(id)
      dispatch({
        type: GET_JOB_DETAIL_SUCCESS,
        jobDetail
      })
    } catch (e) {
      dispatch({
        type: GET_JOB_DETAIL_FAILURE,
        error: e
      })
    }
  }
}

export function setFilters (payload) {
  return dispatch => {
    dispatch({
      type: SET_FILTERS,
      payload
    })
  }
}

export function setLoading (payload) {
  return dispatch => {
    dispatch({
      type: SET_LOADING,
      payload
    })
  }
}
