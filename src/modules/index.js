import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import applications from './applications'
import campings from './campings'
import user from './user'

const middlewares = [thunk]
const store = createStore(
  combineReducers({
    campings: campings,
    applications,
    user
  }),
  applyMiddleware(...middlewares)
)

export default store
