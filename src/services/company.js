import receita from './receita'

/**
 * Get Company data from Receita Federal through it's CNPJ
 * @param cnpj
 * @returns {Promise<T | never>}
 */
export function getCompany (cnpj) {
    return receita.get(`cnpj/${cnpj}`)
        .then(response => {
            return response.data
        })
        .catch(err => {
            throw err
        })
}
