import axios from 'axios'

/**
 * The Place Autocomplete service is a web service that returns place predictions in response to an HTTP request.
 * @type {AxiosInstance}
 */
const GooglePlaces = axios.create({
  baseURL: 'https://maps.googleapis.com/maps/api',
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Accept': 'application/json'
  }
})

export default GooglePlaces
