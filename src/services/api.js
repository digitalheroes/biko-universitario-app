import http from './biko'

export function login (payload) {
  return http.post('auth', payload)
    .then(response => { return response.data })
    .catch(error => { console.error(error) })
}

export function createStudent (payload) {
  return http.post('students', payload)
    .then(response => { return response.data })
    .catch(error => { throw error.response.data })
}

export function createEmployer (employer) {
  return http.post('employers', employer)
    .then(response => { return response.data })
    .catch(error => { throw error.response.data })
}

export function createCompany (payload) {
  return http.post('companies', payload)
    .then(response => { return response.data })
    .catch(error => { throw error.response.data })
}

export async function createApplication (payload) {
  try {
    const { data } = await http.post('applications', payload)
    return data
  } catch (error) {
    const { response: { data } } = error
    throw data
  }
}

/**
 * Get all Jobs
 * @returns {Promise<T | never>}
 */
export async function getJobs () {
  try {
    const { data } = http.get('jobs')
    return data
  } catch (e) {
    throw e
  }
}

export function getJobById (id) {
  return http.get(`jobs/${id}`)
    .then(response => {
      return response.data
    })
    .catch(error => {
      throw error
    })
}

export function searchHabilities (text) {
  return http.get(`habilities/search?query=${text}`)
    .then(response => {
      return response.data
    })
    .catch(error => {
      throw error
    })
}

export function associateHabilitiesToStudent (studentId, habilities) {
  return http.post('students/habilities', { studentId, habilities })
    .then(response => { return response.data })
    .catch(error => { throw error.response.data })
}
