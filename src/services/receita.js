import axios from 'axios'

/**
 * A simple API for retrieving company information through your CNPJ
 * @type {AxiosInstance}
 */
const receitaws = axios.create({
    baseURL: 'https://www.receitaws.com.br/v1/',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
    }
});

// axios.interceptors.response.use((response) => {
//     return Promise.resolve(response)
// }, function (error) {
//     if (error.response.status === 401) {
//         console.log('401 error')
//     }
//     return Promise.reject(error)
// });

export default receitaws
