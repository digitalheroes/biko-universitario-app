import biko from './biko'

/**
 * Get all Jobs
 * @returns {Promise<*>}
 */
export async function getJobs () {
  try {
    const { data } = await biko.get('jobs')
    return data
  } catch (e) {
    throw e
  }
}

export function getJobById (id) {
  return biko.get(`jobs/${id}`)
    .then(response => {
      return response.data
    })
    .catch(error => {
      throw error
    })
}
