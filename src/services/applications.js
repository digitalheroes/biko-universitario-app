import HttpRequestHandler from './biko'

export async function createApplication (payload) {
  try {
    const { data } = await HttpRequestHandler.post('applications', payload)
    return data
  } catch (error) {
    const { response: { data } } = error
    throw data
  }
}
