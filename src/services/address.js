import Postmon from './postmon'
import GooglePlaces from './googlePlaces'

/**
 * Get Address data from Postmon through it's zipCode
 * @param cep
 * @returns {Promise<T | never>}
 */
export function getAddress (cep) {
  return Postmon.get(`cep/${cep}`)
    .then(response => {
      return response.data
    })
    .catch(err => {
      return err
    })
}

export async function getPredictions (query) {
  const { data } = await GooglePlaces.get(
    'place/autocomplete/json',
    { params: {
      input: query,
      key: 'AIzaSyD3DFxMLq9nkvql1otQU8cbgQMrm8j0Erk',
      types: 'geocode'
    } }
  )
  return data
}

export async function getPlaceDetails (placeid) {
  const { data } = await GooglePlaces.get(
    'place/details/json',
    { params: {
      key: 'AIzaSyD3DFxMLq9nkvql1otQU8cbgQMrm8j0Erk',
      placeid,
      fields: 'address_component,geometry'
    } }
  )
  return data
}
