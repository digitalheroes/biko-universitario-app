import HttpRequestHandler from './biko'

export async function getCurrentStudent () {
  try {
    const { data } = await HttpRequestHandler.get('students')
    return data
  } catch (e) {
    const { data } = e.response
    return data
  }
}

export function updateStudent (student) {
  try {
    const { data } = HttpRequestHandler.put('students', student)
    return data
  } catch (e) {
    const { data } = e.response
    return data
  }
}

export async function getStudentApplications (studentId) {
  try {
    const { data } = await HttpRequestHandler.get(`students/${studentId}/applications`)
    return data
  } catch (e) {
    throw e
  }
}
