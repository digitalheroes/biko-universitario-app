import axios, { AxiosInstance } from 'axios'
import { getToken, getUser } from '../helpers'

/**
 * Biko
 * @type {AxiosInstance}
 */
const biko = axios.create({
  baseURL: 'https://biko-app.herokuapp.com/api/'
  // baseURL: 'http://10.0.2.2:5622/api/'
})

biko.interceptors.request.use(
  async config => {
    const requestConfig = config
    let token = null
    try {
      token = await getToken()
    } catch (e) {
      throw e
    }
    if (token) {
      requestConfig.headers.Authorization = `Bearer ${token}`
    }
    return Promise.resolve(requestConfig)
  },
  error => {
    return Promise.reject(error)
  }
)

biko.interceptors.response.use((response) => {
  return Promise.resolve(response)
}, function (error) {
  if (error.response.status === 401) {
    console.log('401 error')
  }
  console.log(error)
  return Promise.reject(error)
})

export default biko
