import axios from 'axios'

/**
 * Postmon an API to query ZIP codes and orders the easy way, fast and free.
 * @type {AxiosInstance}
 */
const postmon = axios.create({
    baseURL: 'https://api.postmon.com.br/v1/',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
    }
});

// axios.interceptors.response.use((response) => {
//     return Promise.resolve(response)
// }, function (error) {
//     if (error.response.status === 401) {
//         console.log('401 error')
//     }
//     return Promise.reject(error)
// });

export default postmon
