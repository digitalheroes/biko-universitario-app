import { Dimensions } from 'react-native';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default {
  window: {
    width,
    height,
  },
  isSmallDevice: width < 375,
};

export const KEYBOARD_AVOIDING_BEHAVIOR = Platform.select({
  ios: 'padding',
  android: 'none',
});

