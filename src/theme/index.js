import { Platform } from 'react-native'

export const fontFamily = Platform.select({
  ios: {
    montserrat: {
      regular: 'Montserrat',
      bold: 'Montserrat',
      light: 'Montserrat'
    },
    palanquin: {
      regular: 'Palanquin',
      medium: 'Palanquin',
      semiBold: 'Palanquin',
      bold: 'Palanquin',
      light: 'Palanquin'
    }
  },
  android: {
    montserrat: {
      regular: 'Montserrat Regular',
      bold: 'Montserrat Bold',
      light: 'Montserrat Light'
    },
    palanquin: {
      regular: 'Palanquin Regular',
      medium: 'Palanquin Medium',
      semiBold: 'Montserrat SemiBold',
      bold: 'Palanquin Bold',
      light: 'Palanquin Light'
    }
  }
})

export const theme = {
  fontFamily
}
