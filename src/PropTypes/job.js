import PropTypes from 'prop-types'

export default PropTypes.shape({
  id: PropTypes.number,
  jobTitle: PropTypes.string,
  jobDescription: PropTypes.string,
  jobRemuneration: PropTypes.number,
  jobPeriod: PropTypes.string,
  jobType: PropTypes.string,
  jobCheckIn: PropTypes.string,
  jobCheckOut: PropTypes.string,
  Company: PropTypes.shape({
    phantasyName: PropTypes.string,
    imageUrl: PropTypes.string,
    companyDescription: PropTypes.string,
    Address: PropTypes.shape({
      latitude: PropTypes.string,
      longitude: PropTypes.string
    }),
    Skills: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number,
      habilityName: PropTypes.string
    }))
  })
})
