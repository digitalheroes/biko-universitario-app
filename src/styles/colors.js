export default {
  white: '#ffffff',
  primary: '#ff674c',
  secondary: '#575366',
  green: '#ddfc74',
  blue: '#067bc2',
  transparent: 'rgba(0, 0, 0, 0)'
}
