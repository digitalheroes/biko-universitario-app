export default {
  title: 26,
  giant: 22,
  big: 18,
  normal: 16,
  small: 14,
  smaller: 12
}
