import React from 'react'
import FlashMessage from 'react-native-flash-message'
import Geocoder from 'react-native-geocoding'
import { Provider } from 'react-redux'
import { ThemeProvider } from 'nachos-ui'
import { Platform, StatusBar, StyleSheet, View } from 'react-native'
import AppNavigator from './navigation/AppNavigator'

import store from './modules'
import { fontFamily } from './theme'

const branding = {
  primaryColor: '#ff674c',
  primaryLightColor: '#FFAA9A',
  secondaryColor: '#37455c',
  linkColor: '#ff674c',
  secondaryLightColor: '#95929F',
  alternateTextColor: '#fff'
}

const theme = {
  Input: {
    style: {
      base: {
        borderRadius: 10,
        marginBottom: 20,
        fontFamily: fontFamily.palanquin.medium,
        fontWeight: '500'
      },
      normal: {
        backgroundColor: '#fff',
        borderColor: 'transparent',
        borderStyle: 'solid',
        color: '#bdc1cc'
      },
      input: {
        fontSize: 18
      },
      icon: {
        top: 12,
        left: 12,
        position: 'absolute',
        backgroundColor: 'transparent'
      }
    }
  },
  Button: {
    props: {
      uppercase: false,
      textStyle: {
        color: '@alternateTextColor',
        fontFamily: fontFamily.palanquin.semiBold,
        fontWeight: '600'
      }
    },
    style: {
      baseText: {
        color: '@alternateTextColor'
      },
      btn_kind_rounded: {
        marginTop: 20,
        borderRadius: 10,
        height: 44
      },
      text_kind_rounded: {
        fontSize: 14
      },
      state_primary: {
        backgroundColor: '@primaryColor'
      }
    }
  },
  Text: {
    style: {
      base: {
        fontFamily: fontFamily.palanquin.medium,
        fontWeight: '500'
      }
    }
  },
  A: {
    style: {
      base: {
        textDecorationLine: 'none'
      }
    }
  },
  H1: {
    style: {
      base: {
        fontFamily: fontFamily.palanquin.bold,
        fontWeight: '700'
      }
    }
  },
  H2: {
    style: {
      base: {
        fontFamily: fontFamily.palanquin.medium,
        fontWeight: '500'
      }
    }
  },
  H3: {
    style: {
      base: {
        fontFamily: fontFamily.palanquin.semiBold,
        fontWeight: '600'
      }
    }
  }
}

export default class App extends React.Component {
  constructor (props) {
    super(props)
    try {
      Geocoder.init('AIzaSyCcZA7d9OoELps1IhwC7OhzLeo9FdmnG28')
    } catch (e) {
      console.error(e)
    }
    FlashMessage.setColorTheme({
      success: '#50E0B5',
      info: '#125BE0',
      warning: '#F5B800',
      danger: '#FF3C54'
    })
  }

  render () {
    return (
      <ThemeProvider theme={theme} branding={branding}>
        <Provider store={store}>
          <View style={styles.container}>
            {Platform.OS === 'ios' && <StatusBar barStyle='default' />}
            <AppNavigator />
          </View>
        </Provider>
        <FlashMessage position='top' floating />
      </ThemeProvider>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  }
})
