import moment from 'moment'
import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, View } from 'react-native'
import { P, H3 } from 'nachos-ui'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import { fontFamily } from '../theme'

const styles = StyleSheet.create({
  card: {
    borderRadius: 10,
    padding: 15,
    backgroundColor: '#FFF',
    marginHorizontal: 10,
    marginVertical: 10,
    shadowRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: { x: 2, y: -2 },
    shadowColor: '#000',
    overflow: 'hidden'
  },
  cardTitle: {
    flex: 1,
    fontFamily: fontFamily.palanquin.regular
  },
  cardMatch: {
    marginHorizontal: 10
  },
  cardInfoContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    flexWrap: 'wrap'
  },
  cardInfoIcon: {
    width: 26,
    marginRight: 10,
    color: '#cecece'
  },
  cardInfoText: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap'
  }
})

const ApplicationListItem = ({ application, navigation }) => {
  const { createdAt, Job: job } = application
  const { Company: company, jobTitle, jobDescription } = job
  const { phantasyName, companyName } = company
  return (
    <View style={styles.card} elevation={6}>
      <View style={styles.textContent}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <H3 numberOfLines={1} style={styles.cardTitle}>{jobTitle}</H3>
        </View>
        <View>
          <P>{jobDescription}</P>
        </View>
        <View>
          <View style={styles.cardInfoContainer}>
            <FontAwesome5 style={styles.cardInfoIcon} name='building' size={26} />
            <P style={styles.cardInfoText} numberOfLines={2}>{phantasyName || companyName}</P>
          </View>
        </View>
        <View>
          <View style={styles.cardInfoContainer}>
            <P style={styles.cardInfoText} numberOfLines={2}>Se candidatou {moment(createdAt).fromNow()}</P>
          </View>
        </View>
      </View>
    </View>
  )
}

ApplicationListItem.propTypes = {
  application: PropTypes.shape({
    id: PropTypes.number,
    applicationMessage: PropTypes.string,
    createdAt: PropTypes.string,
    updatedAt: PropTypes.string,
    Job: PropTypes.shape({
      id: PropTypes.number,
      jobTitle: PropTypes.string,
      jobDescription: PropTypes.string,
      jobRemuneration: PropTypes.number,
      jobPeriod: PropTypes.string,
      jobType: PropTypes.string,
      jobCheckIn: PropTypes.string,
      jobCheckOut: PropTypes.string,
      Company: PropTypes.shape({
        phantasyName: PropTypes.string,
        imageUrl: PropTypes.string,
        companyDescription: PropTypes.string,
        Address: PropTypes.shape({
          latitude: PropTypes.string,
          longitude: PropTypes.string
        }),
        Skills: PropTypes.arrayOf(PropTypes.shape({
          id: PropTypes.number,
          habilityName: PropTypes.string
        }))
      })
    })
  })
}

export default ApplicationListItem
