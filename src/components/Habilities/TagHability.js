import React from 'react'
import { StyleSheet, TouchableOpacity, View } from 'react-native'
import PropTypes from 'prop-types'
import { P } from 'nachos-ui'
import Icon from 'react-native-vector-icons/Feather'
import colors from '../../styles/colors'

const styles = StyleSheet.create({
  tagHabilityContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: colors.primary,
    paddingVertical: 5,
    paddingLeft: 15,
    paddingRight: 10,
    marginVertical: 5,
    marginHorizontal: 5,
    borderRadius: 20
  },
  tagHabilityText: {
    color: colors.white,
    paddingVertical: 0
  },
  tagHabilityRemove: {
    alignSelf: 'flex-end',
    paddingLeft: 10
  }
})

const TagHability = props => {
  return (
    <View style={styles.tagHabilityContainer}>
      <P style={styles.tagHabilityText}>{props.hability.habilityName}</P>
      <TouchableOpacity style={styles.tagHabilityRemove} onPress={props.onPress}>
        <Icon name={'x'} size={24} color={'#ffffff'} />
      </TouchableOpacity>
    </View>
  )
}

TagHability.propTypes = {
  hability: PropTypes.object
}

export default TagHability
