import React from 'react';

import {View} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

import styles from './styles';

const Tab = () => (
    <View style={styles.container}>
        <Icon name="search" size={22} style={styles.icon}/>
        <Icon name="briefcase" size={22} style={[styles.icon, styles.active]}/>
        <Icon name="upload" size={22} style={styles.icon}/>
        <Icon name="settings" size={22} style={styles.icon}/>
    </View>
);

export default Tab;