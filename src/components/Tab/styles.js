import { StyleSheet } from 'react-native';
import { metrics, fonts, colors } from '../../styles';

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.white,
        height: metrics.tabBarHeight,
        paddingHorizontal: metrics.padding,
        borderTopWidth: 1,
        borderColor: '#eeeeee',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    icon: {
        color: '#bbb'
    },
    active: {
        color: colors.primary,
    }
});

export default styles;