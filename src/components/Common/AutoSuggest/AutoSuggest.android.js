import Autocomplete from 'react-native-autocomplete-input'
import React from 'react'
import {StyleSheet, View} from 'react-native'
import styled from 'styled-components'

const AutoSuggestWrapper = styled.View`
	width: 400px;
	flex: 1;
	padding-top: 25px;
`

const styles = StyleSheet.create({
	autocompleteContainer: {
		flex: 1,
		left: 0,
		position: 'absolute',
		right: 0,
		top: 0,
		zIndex: 1
	}
});

const AutoSuggest = props => {
	return (
		<AutoSuggestWrapper>
			<Autocomplete containerStyle={styles.autocompleteContainer} {...props} >
				{props.child}
			</Autocomplete>
		</AutoSuggestWrapper>
	)
}

export default AutoSuggest
