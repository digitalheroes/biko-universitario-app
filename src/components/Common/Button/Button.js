import colors from '../../../styles/colors'
import { Text, TouchableHighlight, View } from 'react-native'
import React from 'react'
import styled from 'styled-components/native'
import fonts from '../../../styles/fonts'

const ButtonContainer = styled.View`
	margin: 15px;
	flex-direction: row;
	align-items:center;
	justify-content: space-between;
`

const TouchableButton = styled.TouchableHighlight`
	flex: 1;
	align-items: center;
	background-color: ${colors.primary};
	height: 48px;
	padding: 10px;
	margin: 0 10px;
	border-radius: 64;
`

const ButtonText = styled.Text`
	text-align: center;
	color: ${colors.white};
	font-size: ${fonts.big};
`

const PrimaryButton = (props) => {
  const { buttonText, ...other } = props
  return (
    <ButtonContainer>
      <TouchableButton underlayColor={'#e85e46'} {...other}>
        <ButtonText>{buttonText}</ButtonText>
      </TouchableButton>
    </ButtonContainer>
  )
}

export default PrimaryButton
