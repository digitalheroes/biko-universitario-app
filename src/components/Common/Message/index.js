import React from 'react';
import * as Animatable from 'react-native-animatable';
import {Text} from 'react-native';

const Message = () => (
    <Animatable.View animation={'bounceIn'}>
        <Text>{this.props.message}</Text>
    </Animatable.View>
);

export default Message
