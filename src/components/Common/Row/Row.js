import React from 'react'
import { Platform, StyleSheet, Text, TouchableHighlight, TouchableNativeFeedback, View } from 'react-native'
import Icon from 'react-native-vector-icons/Feather'
import PropTypes from 'prop-types'
import fonts from '../../../styles/fonts'

const styles = StyleSheet.create({
  touchableRowContainer: {
    paddingHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 65
  },
  rowContainer: {
    flexDirection: 'row'
  },
  rowText: {
    fontSize: fonts.big,
    marginLeft: 15
  }
})

const TouchableRow = props => {
  if (Platform.OS === 'android') {
    return (
      <TouchableNativeFeedback onPress={props.onPress}>
        <View style={styles.touchableRowContainer}>
          {props.children}
        </View>
      </TouchableNativeFeedback>
    )
  } else {
    return (
      <TouchableHighlight onPress={props.onPress} underlayColor='#EEEEEE'>
        <View style={styles.touchableRowContainer}>
          {props.children}
        </View>
      </TouchableHighlight>
    )
  }
}

const Row = props => {
  return (
    <TouchableRow onPress={props.onPress}>
      <View style={styles.rowContainer}>
        <Icon size={24} name={props.iconName} />
        <Text style={styles.rowText}>{props.text}</Text>
      </View>
      <Icon size={24} name='chevron-right' />
    </TouchableRow>
  )
}

Row.propTypes = {
  iconName: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired
}

export default Row
