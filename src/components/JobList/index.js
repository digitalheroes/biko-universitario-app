import React, {Component} from 'react';

import {View, TouchableOpacity} from 'react-native';

import styles from './styles';
import Job from './Job'

export default class JobList extends Component {
    state = {
        jobs: [
            {
                jobId: 1,
                jobReward: "60,00",
                jobQuantity: 3,
                jobTitle: "Sushiman",
                jobDescription: "sushiman, fará pratos para restaurante de comida por quilo, servindo 1x por semana.",
                companyName: "Pepe Grill",
                companyImage: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQpZRwxfCD0puzUAS_U-yn9R8wz5p7itmQImi14WXRQOTmcwutt",
            },
            {
                jobId: 2,
                jobReward: "120,00",
                jobQuantity: 5,
                jobTitle: "Garçom",
                jobDescription: "Garçom, fará pratos para restaurante de comida por quilo, servindo 1x por semana.",
                companyName: "Nellara",
                companyImage: "https://is5-ssl.mzstatic.com/image/thumb/Purple128/v4/56/fa/97/56fa971b-9ceb-f6b6-cee2-d4f52f255ab2/source/256x256bb.jpg",
            },
            {
                jobId: 3,
                jobReward: "87,00",
                jobQuantity: 40,
                jobTitle: "Auxiliar de Cozinha",
                jobDescription: "Uma vez contratados, passam por processos de capacitação específicos, nos quais aprendem sobre os cortes especiais de carne, preparo dos pratos, recebimento de clientes, princípios de higiene e outros padrões adotados pela rede.",
                companyName: "Outback",
                companyImage: "https://www.iguatemifortaleza.com.br/wp-content/uploads/2017/03/outb.png",
            },
        ]
    };

    render() {
        return (
            <View style={styles.container}>

                {this.state.jobs.map(job =>
                    <TouchableOpacity onPress={() => { alert("Detalhes da vaga") } }>
                        <Job key={job.jobId} job={job}/>
                    </TouchableOpacity>    
                )}
                
            </View>
        )
    }
}