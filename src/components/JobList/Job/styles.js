import { StyleSheet } from 'react-native';
import { metrics, fonts, colors } from '../../../styles';

// Job

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.white,
        marginVertical: 5,
        marginHorizontal: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#eeeeee',
        paddingVertical: 10,
    },
    title: {
        color: '#000000',
        fontWeight: 'bold',
        fontSize: fonts.big,
    },
    info: {
        color: '#999999',
        fontSize: fonts.big,
        flexDirection: 'row',
        alignItems: 'center',
    },
    quantity: {
        color: '#000000',
        fontSize: fonts.small,
    },
    reward: {
        color: '#000000',
        fontSize: fonts.small,
    },
    companyInfo: {
        paddingVertical: metrics.padding,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    companyImage: {
        width: 48,
        height: 48,
        borderRadius: 5,
    },
    likeButton: {
        color: '#999999',
    },
});

export default styles;