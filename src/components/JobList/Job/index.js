import React from "react";

import {View, Text, Image, TouchableWithoutFeedback} from 'react-native';
import styles from './styles';
import Icon from "react-native-vector-icons/Feather";

const Job = ({job}) => (
        <View style={styles.container}>
            <Text style={styles.title}>{ job.jobTitle }</Text>
            <Text style={styles.info}>
                <Text style={styles.quantity}>Quantidade de vagas: { job.jobQuantity }</Text> •
                <Text style={styles.reward}> R${ job.jobRemuneration } / h</Text>
            </Text>
            <Text style={styles.description}>{ job.jobDescription }</Text>
            <View style={styles.companyInfo}>
                {/*<Image source={{uri: job.Company.imageUrl}} style={styles.companyImage}/>*/}
                <Text>{job.Company.phantasyName}</Text>
                <Icon name="heart" size={16} style={styles.likeButton}/>
            </View>
        </View>
);

export default Job;