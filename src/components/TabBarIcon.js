import PropTypes from 'prop-types'
import React from 'react'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'

import Colors from '../constants/Colors'

const TabBarIcon = ({ name, focused }) => (
  <FontAwesome5
    name={name}
    size={24}
    color={focused ? Colors.tabIconSelected : Colors.tabIconDefault}
  />
)

TabBarIcon.propTypes = {
  name: PropTypes.string.isRequired,
  focused: PropTypes.bool.isRequired
}

export default TabBarIcon
