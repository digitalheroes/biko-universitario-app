import React from 'react'

import { View, Text } from 'react-native'
import Icon from 'react-native-vector-icons/Feather'

import styles from './styles'

const Filter = () => (
  <View style={styles.container}>

    <Text style={styles.title}>Filtros:</Text>

    <Icon name='filter' size={14} color='red' />

  </View>
)

export default Filter
