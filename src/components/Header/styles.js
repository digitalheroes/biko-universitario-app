import { StyleSheet } from 'react-native';
import { metrics, fonts, colors } from '../../styles';
const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.white,
        height: metrics.headerHeight,
        paddingTop: metrics.headerPadding,
        paddingHorizontal: metrics.padding,
        borderBottomWidth: 1,
        borderBottomColor: '#eeeeee',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        shadowColor: '#0003',
        shadowOffset: {width: 5, height: 0},
        shadowOpacity: 16,
        shadowRadius: 2,
    },

    icon: {
        color: colors.primary,
    },

    title: {
        fontSize: fonts.giant,
        color: '#000000'
    },
});

export default styles;
