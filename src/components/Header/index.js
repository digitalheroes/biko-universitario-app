import React from 'react';

import {View, Text} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';


import styles from './styles';
import {Gravatar} from "nachos-ui";

const Header = () => (
    <View style={styles.container}>
        <Icon style={styles.icon} name="chevron-left" size={22} />
        <Text style={styles.title}>Vagas</Text>
        <Gravatar
            email='contato@dgtalhero.es'
            size={32}
            default='retro'
            circle
        />
    </View>
);

export default Header;
