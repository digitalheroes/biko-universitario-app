import React from 'react'
import PropTypes from 'prop-types'
import { TextInput, Text, View, ViewPropTypes, StyleSheet } from 'react-native'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import {fontFamily} from '../theme'

const styles = StyleSheet.create({
  labelContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  customInputContainer: {
    justifyContent: 'flex-start'
  },
  customTextInputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 32,
    paddingHorizontal: 12,
    marginHorizontal: 0,
    minHeight: 56,
    justifyContent: 'space-between'
  },
  customTextInputContainerError: {
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#ff3c54'
  },
  textError: {
    color: '#ff3c54',
    marginHorizontal: 12
  },
  textInput: {
    fontFamily: fontFamily.palanquin.regular,
    fontSize: 17,
    flexGrow: 1
  }
})

const CustomTextInput = ({
  label,
  error,
  labelAdornment,
  leftAdornment,
  rightAdornment,
  containerStyle,
  ...props
}) => {
  return (
    <View style={[styles.customInputContainer, containerStyle]}>
      {label && (
        <View style={styles.labelContainer}>
          <Text>{label}</Text>
          {error && <Text style={styles.textError}>{error}</Text>}
          {labelAdornment}
        </View>
      )}
      <View style={[styles.customTextInputContainer]} error={error}>
        {leftAdornment}
        <TextInput style={styles.textInput} {...props} />
        {error && (
          <FontAwesome5 name='exclamation-circle' size={16} color='#ff3c54' />
        )}
        {rightAdornment}
      </View>
    </View>
  )
}

CustomTextInput.propTypes = {
  label: PropTypes.string,
  error: PropTypes.string,
  labelAdornment: PropTypes.element,
  leftAdornment: PropTypes.element,
  rightAdornment: PropTypes.element,
  containerStyle: ViewPropTypes.style
}

CustomTextInput.defaultProps = {
  label: undefined,
  error: undefined,
  labelAdornment: null,
  leftAdornment: null,
  rightAdornment: null,
  containerStyle: null
}

export default CustomTextInput
