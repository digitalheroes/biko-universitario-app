import React from 'react'
import { Button as BaseButton } from 'nachos-ui'
import PropTypes from 'prop-types'

const Button = ({ children, loading, ...rest }) => {
  return (
    <BaseButton {...rest}>
      {children}
    </BaseButton>
  )
}

Button.propTypes = {
  loading: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
}

Button.defaultProps = {
  loading: false
}

export default Button
