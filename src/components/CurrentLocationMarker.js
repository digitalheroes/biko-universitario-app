import React, { useState } from 'react'
import { Animated, Easing, StyleSheet, View } from 'react-native'
import { Marker } from 'react-native-maps'

const styles = StyleSheet.create({
  markerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 72,
    height: 72
  },
  markerTrail: {
    zIndex: 2,
    width: 12,
    height: 12,
    borderRadius: 72,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(255, 103, 14, 0.4)',
    borderStyle: 'solid',
    borderWidth: 0.5,
    borderColor: '#ff670e'
  },
  markerDot: {
    position: 'absolute',
    width: 12,
    height: 12,
    borderRadius: 12,
    backgroundColor: '#ff670e'
  }
})

const CurrentLocationMarker = props => {
  const [trailSize] = useState(new Animated.Value(0))
  const [dotSize] = useState(new Animated.Value(0))
  function startAnimation () {
    trailSize.setValue(0)
    dotSize.setValue(0)
    const trailAnimation = Animated.sequence([
      Animated.timing(trailSize, {
        toValue: 1,
        duration: 3000,
        easing: Easing.in,
        useNativeDriver: true
      }),
      Animated.timing(trailSize, {
        toValue: 0,
        duration: 1,
        easing: Easing.linear,
        useNativeDriver: true
      })
    ])
    const dotAnimation = Animated.sequence([
      Animated.timing(dotSize, {
        toValue: 1,
        duration: 500,
        easing: Easing.linear,
        useNativeDriver: true
      }),
      Animated.timing(dotSize, {
        toValue: 0,
        duration: 500,
        easing: Easing.exp,
        useNativeDriver: true
      })
    ])
    const fullAnimation = Animated.sequence([dotAnimation, trailAnimation])
    Animated.loop(fullAnimation).start()
  }
  startAnimation()
  return (
    <Marker {...props}>
      <View style={styles.markerContainer}>
        <Animated.View
          style={[
            {
              transform: [
                {
                  scaleX: trailSize.interpolate({
                    inputRange: [0, 1],
                    outputRange: [1, 6]
                  })
                },
                {
                  scaleY: trailSize.interpolate({
                    inputRange: [0, 1],
                    outputRange: [1, 6]
                  })
                }
              ],
              opacity: trailSize.interpolate({
                inputRange: [0, 1],
                outputRange: [0.8, 0]
              })
            }, styles.markerTrail
          ]}
        />
        <Animated.View
          style={[
            {
              transform: [
                {
                  scaleX: dotSize.interpolate({
                    inputRange: [0, 1],
                    outputRange: [1, 1.4]
                  })
                },
                {
                  scaleY: dotSize.interpolate({
                    inputRange: [0, 1],
                    outputRange: [1, 1.4]
                  })
                }
              ]
            }, styles.markerDot
          ]}
        />
      </View>
    </Marker>
  )
}

CurrentLocationMarker.propTypes = Marker.propTypes

export default CurrentLocationMarker
