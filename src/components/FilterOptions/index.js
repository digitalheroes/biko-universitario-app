import React, { Component } from 'react';

import { View, Text, TouchableOpacity } from 'react-native';

import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import Icon from 'react-native-vector-icons/MaterialIcons'

import styles from './styles';

export default class FilterOptions extends Component {

    constructor(props){
        super(props)
        this.state = {

            selectedItems: [],

        }
    }

    onSelectedItemsChange = (selectedItems) => {

        console.log(selectedItems)

        this.setState({ selectedItems })
        
    }

    render() {
    return(

        <View style={styles.optionsContainer}>

        <SectionedMultiSelect
            items={this.props.items} 
            confirmText='Confirmar'
            uniqueKey='id'
            subKey='children'
            selectText={this.props.text}
            hideSearch={true}
            colors={{ primary: '#de541e'}}
            alwaysShowSelectText={true}
            highlightChildren={true}
            selectedIconComponent={<Icon style={{
                fontSize: 18,
                marginHorizontal: 6,
            }}>check</Icon>}
            showDropDowns={false}
            readOnlyHeadings={true}
            onSelectedItemsChange={this.onSelectedItemsChange}
            selectedItems={this.state.selectedItems}
        />

        </View>

     )

    }
   
};