import React from 'react'
import {
  ActivityIndicator,
  View
} from 'react-native'

class Loading extends React.Component {
  // Render any loading content that you like here
  render () {
    return (
      <View style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center'
      }}>
        <ActivityIndicator />
      </View>
    )
  }
}

export default Loading
