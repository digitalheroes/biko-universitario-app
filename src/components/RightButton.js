import React from 'react'
import { ActivityIndicator, StyleSheet, Text, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import colors from '../styles/colors'
import fonts from '../styles/fonts'
import { fontFamily } from '../theme'

const styles = StyleSheet.create({
  saveText: {
    fontWeight: '500',
    fontFamily: fontFamily.palanquin.regular,
    fontSize: fonts.normal,
    color: colors.primary
  },
  saveButton: {
    marginRight: 20
  }
})

const RightButton = ({ title, loading, ...props }) => (
  <TouchableOpacity style={styles.saveButton} {...props}>
    {loading ? (
      <ActivityIndicator color={colors.primary} size={26} />
    ) : (
      <Text style={styles.saveText}>{title}</Text>
    )}
  </TouchableOpacity>
)

RightButton.propTypes = {
  title: PropTypes.string.isRequired,
  loading: PropTypes.bool
}

RightButton.defaultProps = {
  loading: false
}

export default RightButton
