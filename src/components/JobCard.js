import React from 'react'
import PropTypes from 'prop-types'
import job from '../PropTypes/job'
import { StyleSheet, View } from 'react-native'
import { Badge, P, H3, Button } from 'nachos-ui'
import AntDesign from 'react-native-vector-icons/AntDesign'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import {fontFamily} from '../theme'

const styles = StyleSheet.create({
  card: {
    borderRadius: 10,
    padding: 15,
    backgroundColor: '#FFF',
    marginHorizontal: 10,
    marginVertical: 10,
    shadowRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: { x: 2, y: -2 },
    shadowColor: '#000',
    width: 320,
    overflow: 'hidden'
  },
  cardTitle: {
    flex: 1,
    fontFamily: fontFamily.palanquin.regular
  },
  cardMatch: {
    marginHorizontal: 10
  },
  cardInfoContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    flexWrap: 'wrap'
  },
  cardInfoIcon: {
    width: 26,
    marginRight: 10,
    color: '#cecece'
  },
  cardInfoText: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap'
  }
})

const JobCard = ({ job, navigation }) => {
  const { Company: company, jobTitle, match } = job
  const { Address: address, phantasyName, companyName } = company
  const { publicPlace, neighborhood, city, state, streetNumber, zipCode } = address
  const formattedAddress = `${publicPlace}, ${streetNumber} - ${neighborhood}, ${city}, ${state} - ${zipCode}`
  return (
    <View style={styles.card} elevation={6}>
      <View style={styles.textContent}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <H3 numberOfLines={1} style={styles.cardTitle}>{jobTitle}</H3>
          {match && (
            <Badge value='Match' color='#8158d8' style={styles.cardMatch} />
          )}
        </View>
        <View style={{ marginBottom: 20 }}>
          <View style={styles.cardInfoContainer}>
            <AntDesign style={styles.cardInfoIcon} name='enviroment' size={26} />
            <P style={styles.cardInfoText} numberOfLines={2}>{formattedAddress}</P>
          </View>
          <View style={styles.cardInfoContainer}>
            <FontAwesome5 style={styles.cardInfoIcon} name='building' size={26} />
            <P style={styles.cardInfoText} numberOfLines={2}>{phantasyName || companyName}</P>
          </View>
        </View>
      </View>
      <Button onPress={() => navigation.navigate('JobApplication', { job })}>Candidatar-se</Button>
    </View>
  )
}

JobCard.propTypes = {
  job
}

export default JobCard
