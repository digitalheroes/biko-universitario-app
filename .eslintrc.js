module.exports = {
  parser: 'babel-eslint',
  env: {
    browser: true,
    es6: true,
    jest: true
  },
  extends: ["standard", "standard-react"],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
    "__DEV__": true,
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: [
    'react',
  ],
  rules: {
    "react/jsx-filename-extension": ["error", { "extensions": [".js", ".jsx"] }],
    "react/prop-types": ["error", { "ignore": ["navigation"] }],
    "import/prefer-default-export": 0,
    "import/no-named-as-default": 0,
    "import/no-unresolved": 0,
    "no-nested-ternary": 0,
    "consistent-return": 0,
    "array-callback-return": 0,
    "promise/prefer-await-to-callbacks": "error",
    "promise/prefer-await-to-then": "error",
    "import/extensions": [".js", ".jsx"],
    "import/no-extraneous-dependencies": 0
  },
};
